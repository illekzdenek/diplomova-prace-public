import "@4tw/cypress-drag-drop";
import cypress from "cypress";
import { takeRight } from "cypress/types/lodash";

describe("Open app", () => {
  it("App is running", () => {
    cy.visit("http://localhost:3000");
    cy.saveLocalStorage();
  });
  it("Login", () => {
    cy.restoreLocalStorage();
    // cy.get("input[type='text']").type("login"); 
    // cy.get("input[type='password']").type("password"); 
    cy.get(".login-input-email").type("a@a.a")
    cy.get(".login-input-password").type("testovaciHeslo")
    cy.get("button").contains("Login").click();
    cy.saveLocalStorage();
  });
});

const dataTransfer = new DataTransfer();
// describe('Setup flow', () => {
// cy.viewport(1920, 1080)

describe("Initial localStorage", () => {
  it("Settings", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("acceptModalAllowed", true);
      expect(content).that.has.property("autoSaveOnSwitch", true);
      expect(content).that.has.property("displayActionButtons", true);
      expect(content).that.has.property("displayModals", false);
      expect(content).that.has.property("lang", "en");
      expect(content).that.has.property("numberOfTabs", 2);
    });
    cy.saveLocalStorage();
  });

  
  it("Onboarding", () => {

    
    // cy.get(".on-boarding-x").contains("X").click()
    // cy.get(".accept-ok").click()
    
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    cy.get(".on-boarding-next").contains("Next").click()
    
    cy.get(".on-boarding-tutorial").click()
    
    cy.get(".on-boarding-x").contains("X").click()
    // cy.wait(500)
    cy.get(".accept-ok").click()
    
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()

    
    
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Next").click()
    // cy.get(".on-boarding-next").contains("Finish").click()
  });

  it("Tabs", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("tabs").then((tabs) => {
      const content = JSON.parse(tabs);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("array")
        .that.has.length(1)
        .that.include("Tab 1");
    });
    cy.saveLocalStorage();
  });

  // it("Flow tab", () => {
  //   cy.restoreLocalStorage();
  //   cy.getLocalStorage("flow-Tab 1").then((tab) => {
  //     const content = JSON.parse(tab);
  //     expect(content).to.not.undefined;
  //     console.log(content)
  //     // expect(content).to.be.an("object");
  //     expect(content).that.has.property("nodes").that.has.length(0);
  //     expect(content).that.has.property("edges").that.has.length(0);
  //     expect(content)
  //       .that.has.property("position")
  //       .that.is.an("array")
  //       .that.has.length(2);
  //     expect(content.position[0]).equal(0);
  //     expect(content.position[1]).equal(0);
  //     expect(content).that.has.property("zoom", 1);
  //   });
  //   cy.saveLocalStorage();
  // });
});

describe("Tabs setup", () => {
  // it("Is + button unavailable?", () => {
  //   cy.get(".add-tab-button").should("not.exist");
  // });

  it("Open menu", () => {
    cy.get("button span.anticon.anticon-menu").click();
  });

  it("Set maximum tabs to 4", () => {
    cy.restoreLocalStorage();
    cy.get("#settings-numberOfTabs input.ant-input-number-input")
      .clear()
      .type("4");
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("numberOfTabs", 4);
    });
    cy.saveLocalStorage();
  });

  it("Close menu", () => {
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });

  it("Is + button available now?", () => {
    cy.get(".add-tab-button").should("exist");
  });

  it("Add maximum number of tabs", () => {
    cy.restoreLocalStorage();
    cy.get(".add-tab-button").click().click().click();
    cy.getLocalStorage("tabs").then((tabs) => {
      const content = JSON.parse(tabs);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("array")
        .that.has.length(4)
        .that.include("Tab 1");
      expect(content).include("Tab 2");
      expect(content).include("Tab 3");
      expect(content).include("Tab 4");
      expect(content).not.include("Tab 5");
    });
    cy.saveLocalStorage();
  });

  it("Is + button unavailable again?", () => {
    cy.get(".add-tab-button").should("not.exist");
  });
});

describe("Create input node", () => {
  it("Open node menu", () => {
    cy.get("button").contains("Open tasks menu").click();
  });

  it("Start dragging input node", () => {
    //drag and drop start
    
    cy.get("#inputs-dropdown > ul > .dragable-node-input-server-set").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
    // cy.get("#inputs-dropdown > ul > .dragable-node-input-preset").trigger(
    //   "dragstart",
    //   {
    //     dataTransfer,
    //   }
    // );
  });

  it("Drop on canvas", () => {
    // cy.restoreLocalStorage();
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
    // cy.saveLocalStorage();
    //drag and drop end
  });
  
  it("Move output node", () => {
    cy.get("#dndnode_1_flow-Tab-4 .node-header")
      .trigger("mousedown", "center", { force: true })

      .trigger("mousemove", { clientX: 550, clientY: 500 })
      .trigger("mouseup");
  });
});

describe("Change handle error type", () => {

  it("Open node details", () => {
    cy.get("#dndnode_1_flow-Tab-4").dblclick()
  });

  it("Open settings", () => {
    cy.get("#dndnode_1_flow-Tab-4-drawer-settings").click()
    cy.get("#dndnode_1_flow-Tab-4-error-handling-type").click()
  });

  it("Select option", () => {
    cy.restoreLocalStorage();
    cy.get(".ant-select-item-option-content").contains("ERROR_HANDLING_STOP").click()
    // cy.get(".save__controls button span").contains("Save").click();
    
    cy.get("body").type("{ctrl}s");

    cy.saveLocalStorage();
  });

  
  it("Cancel", () => {
    cy.get(".ant-drawer-content button").contains("Cancel").click();
  });

  
  it("Saving", () => {
    cy.restoreLocalStorage();
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.has.length(1);
      expect(content.nodes[0].data.logicNode.errorHandlingType).to.is.equal("ERROR_HANDLING_STOP")
    });
    cy.saveLocalStorage();
  });
  
});

describe("Size handle change", () => {
  it("Default size", () => {
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").should(
      "have.css",
      "width",
      "11.2567138671875px"
    );
  });
  it("Set different handle size", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-handleSize input.ant-input-number-input")
      .clear()
      .type("20");
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
  it("New size", () => {
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").should(
      "have.css",
      "width",
      "56.2838134765625px"
    );
  });

  it("Set default handle size", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-handleSize input.ant-input-number-input")
      .clear()
      .type("4");
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
});

describe("Create tokenize node", () => {
  it("Create output node", () => {
    cy.get("button").contains("Open tasks menu").click();
    cy.get("#prep-dropdown").click();
  });

  it("Start dragging tokenize node", () => {
    cy.get("#prep-dropdown ul .dragable-node-prep-tokenize").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
  });

  it("Drop on canvas", () => {
    cy.restoreLocalStorage();
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

   
  // it("Saving", () => {
  //   cy.restoreLocalStorage();
  //   cy.get(".save__controls button span").contains("Save").click();
  //   cy.saveLocalStorage();
  // });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.has.length(2);
    });
    cy.saveLocalStorage();
  });

  it("Move tokenize node", () => {
    cy.get("#dndnode_2_flow-Tab-4 .node-header")
      .trigger("mousedown", "center", { force: true })

      .trigger("mousemove", { clientX: 1200, clientY: 500 })
      .trigger("mouseup");
  });
});


describe("Change task settings", () => {
  
  it("Change language - tokenize task", () => {
    cy.restoreLocalStorage();
    // Změny je potřeba do localstorage rovnou takto uložit
    cy.get("#dndnode_2_flow-Tab-4-selectLanguage").click()
    cy.get(".ant-select-item-option-content").contains("Czech").click()
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  
  // it("Saving", () => {
  //   cy.restoreLocalStorage();
  //   cy.get(".save__controls button span").contains("Save").click();
  //   cy.saveLocalStorage();
  // });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.has.length(2);
      expect(content.nodes[1].data.logicNode.language).to.is.equal("czech")
    });
    cy.saveLocalStorage();
  });

});

describe("Upload dataset", () => {

  it("Open upload modal", () => {
    cy.get("#upload-button").click()
  });
  it("Set name of dataset", () => {
    cy.get(".upload-modal input[type='text']").type("CypressAutoUploadDataset")
  });
  it("Upload file", () => {
    const app = './testovaci_soubory/testovaciSOubor.txt'
    cy.get(".upload-modal input[type='file']").selectFile(app,{force:true})
    cy.intercept('https://mta.pef.mendelu.cz/lab-api/files/?name=CypressAutoUploadDataset&header_row=0&skip_rows=0&skip_empty_rows=true&delimiter=%2C').as("fileUploaded")
    cy.wait("@fileUploaded").its("response.statusCode").should("eq", 200)
    
  });
  
  it("Close upload modal", () => {
    cy.get(".upload-modal button").contains("OK").click()
  });
});

describe("Select dataset", () => {
  
  it("Select data", () => {
    cy.get("#dndnode_1_flow-Tab-4server-input-node").click()
    // cy.pause()
  });
  it("Select data", () => {
    cy.get(".ant-select-item.ant-select-item-option.ant-select-item-option-active").click()
  });

});

describe("Connecting dataset to tokenize node", () => {
  it("Connect", () => {
    //connecting two nodes
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']")
      .trigger("mousemove")
      .trigger("mouseup");
  });
  
  it("Saving", () => {
    cy.restoreLocalStorage();
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });
});

describe("Saving", () => {
  it("Saving", () => {
    cy.restoreLocalStorage();
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.not.equal([]);
      expect(content).that.has.property("edges").that.not.equal([]);
    });
    cy.saveLocalStorage();
  });
});

describe("Input copy node, node status & connection status", () => {
  // it("Open node menu", () => {
  //   cy.get("div").contains("Open tasks menu").click();
  // });
  it("Start drag input pos tagger node", () => {
    cy.get("#prep-dropdown ul .dragable-node-prep-pos").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
  });

  it("Drop on canvas", () => {
    cy.restoreLocalStorage();
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
    cy.saveLocalStorage();
  });

  it("Input copy node state - initial", () => {
    // cy.log("-------Dot should be blue-------")
    cy.get("#dndnode_3_flow-Tab-4 .dot-alpha")
      .should("have.css", "background-color", "rgb(173, 216, 230)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Ready to work.");
    cy.get("#dndnode_3_flow-Tab-4 .dot-alpha").trigger("mouseleave");
    cy.wait(500);
  });

  it("Input copy node move", () => {
    cy.get("#dndnode_3_flow-Tab-4 .node-header")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1600, clientY: 400 })
      .trigger("mouseup");
  });

  it("Tokenize do not run autorun", () => {
    // cy.log("-------Dot should be green-------")
    cy.get("#dndnode_2_flow-Tab-4 .dot-alpha")
      .should("have.css", "background-color", "rgb(173, 216, 230)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Ready to work.");
    cy.get("#dndnode_2_flow-Tab-4 .dot-alpha").trigger("mouseleave");
    cy.wait(500);
  });
  it("Grab edge from input copy node - output handler", () => {
    cy.get("div[data-test-id='dndnode_3_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
  });

  it("Connection status - connection formats are NOT compatible - multiple connections not allowed", () => {
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']")
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(255, 0, 0)");
  });

  it("Tokenize status check ready to work", () => {
    cy.get("#dndnode_2_flow-Tab-4 .dot-alpha")
      .should("have.css", "background-color", "rgb(173, 216, 230)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Ready to work.");
    cy.get("#dndnode_2_flow-Tab-4 .dot-alpha").trigger("mouseleave");
    cy.wait(500);
  });

  it("From input to output connection", () => {
    cy.restoreLocalStorage();
    //error connection color check
    cy.get("div[data-test-id='dndnode_3_flow-Tab-4-a-input']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-output']")
      // vue-flow__handle vue-flow__handle-left vue-flow__handle-a nodrag target connectable
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(173, 255, 47)");
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-output']").trigger(
      "mouseup"
    );
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });
  
  // it("Saving", () => {
  //   cy.restoreLocalStorage();
  //   cy.get(".save__controls button span").contains("Save").click();
  //   cy.saveLocalStorage();
  // });
});


// describe("Run pipeline", () => {
  
//   it("Press button", () => {
//     cy.get(".save__controls button span").contains("Run pipeline").click();
//     cy.wait(50);
//   });
  
  
//   it("Check tokenize done", () => {

//     cy.get("#dndnode_2_flow-Tab-4 .dot-alpha")
//       .should("have.css", "background-color", "rgb(0, 128, 0)")
    
//     cy.get("#dndnode_3_flow-Tab-4 .dot-alpha")
//       .should("have.css", "background-color", "rgb(173, 216, 230)")
//     cy.wait(500);
//   });
  
//   it("Check remove stop words done", () => {
  
//     cy.get("#dndnode_3_flow-Tab-4 .dot-alpha")
//       .should("have.css", "background-color", "rgb(0, 128, 0)")
//     cy.wait(500);
//   });
// });

// describe("POS Process check", () => {
//   it("Original data", () => {
//     //test footer panel data
//     cy.get(
//       "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(1) > span > span"
//     ).should("have.text", "Propad");
//     cy.get(
//       "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(2) > span > span"
//     ).should("have.text", "přišel");
//     cy.get(
//       "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td > span > span:nth-child(1) > span > span"
//     ).should("have.text", "Jaké");
//   });

//   it("POS tagged data", () => {
//     cy.get(
//       "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(1)"
//     ).should("have.html", '<span data-v-47b67262=""> [<span class="ant-tag" data-v-47b67262="">Propad<!----></span> - NOUN ] </span>');
//     cy.get(
//       "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(2)"
//     ).should("have.html", '<span data-v-47b67262=""> [<span class="ant-tag" data-v-47b67262="">přišel<!----></span> - NOUN ] </span>');
//     cy.get(
//       "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td > span > span:nth-child(1)"
//     ).should("have.html", '<span data-v-47b67262=""> [<span class="ant-tag" data-v-47b67262="">Jaké<!----></span> - NOUN ] </span>');
//   });
// });



describe("Flow position", () => {
  it("Moving with flow (background)", () => {
    cy.restoreLocalStorage();
    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Scroll flow (background)", () => {
    cy.restoreLocalStorage();
    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Moved and zoomed", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content).to.be.an("object").that.has.property("nodes");
      expect(content).that.has.property("edges");
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2)
        // .to.have.members([250, 245.5]);
        .to.have.members([-485.5371210344442, -88.87164346262836])
      expect(content).that.has.property("zoom").that.equal(0.5);
    });
    cy.saveLocalStorage();
  });
});

describe("Remove last edge", () => {

  
  it("Actual localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("edges")
        .that.has.length(2);
    });
    cy.saveLocalStorage();
  });
  
  it("Remove last edge", () => {
    cy.restoreLocalStorage();
    cy.get("#dndnode_2_flow-Tab-4-dndnode_3_flow-Tab-4").click()
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  
  // it("Saving", () => {
  //   cy.restoreLocalStorage();
  //   cy.get(".save__controls button span").contains("Save").click();
  //   cy.saveLocalStorage();
  // });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("edges")
        .that.has.length(1);
    });
    cy.saveLocalStorage();
  });

});

describe("Remove node", () => {

  
  it("Actual localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.has.length(3);
    });
    cy.saveLocalStorage();
  });
  
  it("Remove tokenize node", () => {
    cy.restoreLocalStorage();
    cy.get("#dndnode_2_flow-Tab-4-remove-button").click()
    cy.get(".save__controls button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  
  // it("Saving", () => {
  //   cy.restoreLocalStorage();
  //   cy.get(".save__controls button span").contains("Save").click();
  //   cy.saveLocalStorage();
  // });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      console.log(content)
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("edges")
        .that.has.length(0);
      expect(content)
          .that.has.property("nodes")
          .that.has.length(2);
    });
    cy.saveLocalStorage();
  });

});

describe("Language change", () => {
  it("Language is set to english", () => {
    //change language
    cy.get("#log > div.header > h3").should("have.text", "Log:");
  });

  it("Language switch", () => {
    cy.get(".ant-switch .ant-switch-inner").contains("CS").click();
  });

  it("Check language changed to czech", () => {
    cy.get("#log > div.header > h3").should("have.text", "Historie:");
  });

  it("Language switch back", () => {
    cy.get(".ant-switch .ant-switch-inner").contains("EN").click();
  });

  it("Check language changed back to english", () => {
    cy.get("#log > div.header > h3").should("have.text", "Log:");
  });
});

describe("Accept modal", () => {
  it("Cancel", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#menu-language-switch > button").click();

    // cy.get(".drawer-header .drawer-header button span.anticon.anticon-close").click()
  });
  it("Cancel", () => {
    cy.get(".ant-modal-content button").contains("Cancel").click();
    cy.get(".ant-drawer-body h1").should("have.text", "Settings");
  });
  it("Trigger accept modal", () => {
    cy.get("#menu-language-switch > button").click();
  });
  it("Ok", () => {
    cy.get(".ant-modal-content button").contains("Ok").click();
    cy.get(".ant-drawer-body h1").should("have.text", "Nastavení");
  });

  it("Disable accept modal", () => {
    cy.get("#settings-acceptModalAllowed > button").click();
  });

  it("Try to trigger accept modal", () => {
    cy.get("#menu-language-switch > button").click();
  });

  it("Accept modal exist?", () => {
    cy.get(".ant-modal-mask").should("have.css", "display", "none");

    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
});

describe("Keyboard events", () => {
  it("ctrl + s", () => {
    cy.get("body").type("{ctrl}s");

    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });
  it("The rest of tests", () => {});
});

describe("AutoSaving when switching tabs", () => {
  it("Switching between flows auto saving", () => {
    cy.restoreLocalStorage();
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 3']"
    ).click();
    cy.get("div").contains("Open tasks menu").click();
    // cy.get("div").contains("Otevřít nabídku úloh").click()

    cy.get("#inputs-dropdown ul .dragable-node-input-server-set").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );

    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });

    cy.get("#dndnode_4_flow-Tab-3 .node-header")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1000, clientY: 100 })
      .trigger("mouseup");

    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });

    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });

    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 2']"
    ).click();

    cy.getLocalStorage("flow-Tab 3").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("nodes").that.has.length(1);
      expect(content).that.has.property("edges").that.has.length(0);
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2);
      expect(content.position[0]).equal(-945.5371210344442);
      expect(content.position[1]).equal(12.128356537371644);
      expect(content).that.has.property("zoom", 0.5);
    });

    cy.wait(500);

    cy.saveLocalStorage();
  });

  it("Switching between tabs turned off", () => {
    cy.restoreLocalStorage();
    //turn off save on switch
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-autoTabSave > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();

    //check if saved settings changed
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("autoSaveOnSwitch", false);
    });
    //switch on tab 2
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 2']"
    ).click();
    cy.get("div").contains("Open tasks menu").click();
    // cy.get("div").contains("Otevřít nabídku úloh").click()

    //add node
    cy.get("#inputs-dropdown ul .dragable-node-input-server-set").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );

    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });

    //move node
    cy.get("#dndnode_5_flow-Tab-2 .node-header")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1000, clientY: 100 })
      .trigger("mouseup");

    //zoom
    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });

    //move with flow
    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });

    //switch to tab 1
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 1']"
    ).click();

    //check if tab 2 is not saved
    cy.getLocalStorage("flow-Tab 2").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("nodes").that.has.length(0);
      expect(content).that.has.property("edges").that.has.length(0);
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2);
      expect(content.position[0]).equal(-614.0090234299646);
      expect(content.position[1]).equal(-387.78273752030907);
      expect(content).that.has.property("zoom", 0.703547834860997);
    });

    cy.saveLocalStorage();
  });
});


describe("Tokenize Process check", () => {
  
  it("Original data", () => {
    
    //switch to tab 1
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 1']"
    ).click();

    cy.get(".vue-flow__controls-fitview").click()

    cy.get("#dndnode_2_flow-Tab-1").click()

    //test footer panel data
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span"
    ).should("have.text", "This is saved dataset.");
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td > span"
    ).should("have.text", "This is second line in saved dataset.");
  });

  it("POS tagged data", () => {
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(1) > span > span"
    ).should("have.text", "This");
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td > span > span:nth-child(2) > span > span"
    ).should("have.text", 'is');
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td > span > span:nth-child(1) > span > span"
    ).should("have.text", 'This');
  });
});
