import "@4tw/cypress-drag-drop";
import { takeRight } from "cypress/types/lodash";

describe("Open app", () => {
  it("App is running", () => {
    cy.visit("http://localhost:3000");
    cy.saveLocalStorage();
  });
  it("Login", () => {
    cy.restoreLocalStorage();
    cy.get("input[type='text']").type("login"); //.contains('About')
    cy.get("input[type='password']").type("password"); //.contains('About')
    cy.get("button").contains("Login").click();
    cy.saveLocalStorage();
  });
});

const dataTransfer = new DataTransfer();
// describe('Setup flow', () => {
// cy.viewport(1920, 1080)

describe("Initial localStorage", () => {
  it("Settings", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("acceptModalAllowed", true);
      expect(content).that.has.property("autoSaveOnSwitch", true);
      expect(content).that.has.property("displayActionButtons", true);
      expect(content).that.has.property("displayModals", false);
      expect(content).that.has.property("lang", "en");
      expect(content).that.has.property("numberOfTabs", 1);
    });
    cy.saveLocalStorage();
  });

  it("Tabs", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("tabs").then((tabs) => {
      const content = JSON.parse(tabs);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("array")
        .that.has.length(1)
        .that.include("Tab 1");
    });
    cy.saveLocalStorage();
  });

  it("Flow tab", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("flow-Tab 1").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("nodes").that.has.length(0);
      expect(content).that.has.property("edges").that.has.length(0);
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2);
      expect(content.position[0]).equal(0);
      expect(content.position[1]).equal(0);
      expect(content).that.has.property("zoom", 1);
    });
    cy.saveLocalStorage();
  });
});

describe("Tabs setup", () => {
  it("Is + button unavailable?", () => {
    cy.get(".add-tab-button").should("not.exist");
  });

  it("Open menu", () => {
    cy.get("button span.anticon.anticon-menu").click();
  });

  it("Set maximum tabs to 4", () => {
    cy.restoreLocalStorage();
    cy.get("#settings-numberOfTabs input.ant-input-number-input")
      .clear()
      .type("4");
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("numberOfTabs", 4);
    });
    cy.saveLocalStorage();
  });

  it("Close menu", () => {
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });

  it("Is + button available now?", () => {
    cy.get(".add-tab-button").should("exist");
  });

  it("Add maximum number of tabs", () => {
    cy.restoreLocalStorage();
    cy.get(".add-tab-button").click().click().click();
    cy.getLocalStorage("tabs").then((tabs) => {
      const content = JSON.parse(tabs);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("array")
        .that.has.length(4)
        .that.include("Tab 1");
      expect(content).include("Tab 2");
      expect(content).include("Tab 3");
      expect(content).include("Tab 4");
      expect(content).not.include("Tab 5");
    });
    cy.saveLocalStorage();
  });

  it("Is + button unavailable again?", () => {
    cy.get(".add-tab-button").should("not.exist");
  });
});

describe("Create input node", () => {
  it("Open node menu", () => {
    cy.get("button").contains("Open tasks menu").click();
  });

  it("Start dragging input node", () => {
    //drag and drop start
    cy.get("#inputs-dropdown > ul > .dragable-node-input-preset").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
  });

  it("Drop on canvas", () => {
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
    //drag and drop end
  });
});

describe("Display actions on node", () => {
  it("Turn off display actions", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-actionButtons > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });

  it("Try find action button", () => {
    cy.get(
      "#dndnode_1_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).should("not.exist");
    // cy.get("#dndnode_1_flow-Tab-4 button span").contains("ADD_DATA").click()
  });

  it("Turn on display actions", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-actionButtons > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });

  it("Try find action button", () => {
    cy.get("#dndnode_1_flow-Tab-4 button span.anticon.anticon-caret-right")
      .should("exist")
      .click();
    cy.get("#dndnode_1_flow-Tab-4 button span")
      .contains("ADD_DATA")
      .should("exist");
  });
});

describe("Size handle change", () => {
  it("Default size", () => {
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").should(
      "have.css",
      "width",
      "16px"
    );
  });
  it("Set different handle size", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-handleSize input.ant-input-number-input")
      .clear()
      .type("20");
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
  it("New size", () => {
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").should(
      "have.css",
      "width",
      "80px"
    );
  });

  it("Set default handle size", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-handleSize input.ant-input-number-input")
      .clear()
      .type("4");
    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
});

describe("Create output node", () => {
  it("Create output node", () => {
    cy.get("button").contains("Open tasks menu").click();
    cy.get("#outputs-dropdown").click();
  });

  it("Start dragging output node", () => {
    cy.get("#outputs-dropdown ul .dragable-node-output-console-log").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
  });

  it("Drop on canvas", () => {
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
  });

  it("Move output node", () => {
    cy.get("#dndnode_2_flow-Tab-4 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center", { force: true })

      .trigger("mousemove", { clientX: 1500, clientY: 500 })
      .trigger("mouseup");
  });
});

describe("Input node get & set data", () => {
  it("Set data to node", () => {
    cy.get(
      "#dndnode_1_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_1_flow-Tab-4 button span").contains("ADD_DATA").click();
  });

  it("Check data is corectly set up", () => {
    cy.get(
      "#dndnode_1_flow-Tab-4 > div.node-footer > div:nth-child(1) > button > span"
    ).click();
    // cy.get("body > div:nth-child(6) > div > div.ant-drawer-content-wrapper > div > div > div.ant-drawer-body > ul > li:nth-child(1) > div").click()
    cy.get("#dndnode_1_flow-Tab-4-drawer-data").click();
    cy.get(
      "#dndnode_1_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div"
    ).should("have.text", "B");
    cy.get(
      "#dndnode_1_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > div"
    ).should("have.text", "51");
    cy.get(
      "#dndnode_1_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(3)"
    ).should("have.text", "driver");
  });

  it("Line chart can set & show data", () => {
    cy.get("#dndnode_1_flow-Tab-4-drawer-bar-chart").click();
    // cy.wait(1500)
    cy.get("#node-dndnode_1_flow-Tab-4-canvas-bar-chart");
    cy.get("#node-dndnode_1_flow-Tab-4-canvas-bar-chart-select-1")
      .scrollIntoView()
      .click({ force: true });
    // cy.get("body > div:nth-child(7) > div > div > div > div.rc-virtual-list > div.rc-virtual-list-holder > div > div > div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div").click()
    cy.get(".ant-select-item-option-content")
      .contains("name")
      .scrollIntoView()
      .click({ force: true });

    cy.get("#node-dndnode_1_flow-Tab-4-canvas-bar-chart-select-2")
      .scrollIntoView()
      .click({ force: true });
    // cy.get("body > div:nth-child(8) > div > div > div > div.rc-virtual-list > div.rc-virtual-list-holder > div > div > div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div").click()
    cy.get(".ant-select-item-option-content")
      .contains("age")
      .scrollIntoView()
      .click({ force: true });

    cy.get("#node-dndnode_1_flow-Tab-4-canvas-bar-chart-select-3")
      .scrollIntoView()
      .click({ force: true });
    // cy.get("body > div:nth-child(9) > div > div > div > div.rc-virtual-list > div.rc-virtual-list-holder > div > div > div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div").click()
    cy.get(".ant-select-item-option-content")
      .contains("default")
      .scrollIntoView()
      .click({ force: true });

    cy.get(
      "body > div:nth-child(6) > div > div.ant-drawer-content-wrapper > div > div > div.ant-drawer-footer > button > span"
    )
      .scrollIntoView()
      .click({ force: true });
  });

  it("Close detail", () => {
    cy.get("#dndnode_1_flow-Tab-4-drawer-cancel-button").click();
  });
});

describe("Modals instead of node drawer", () => {
  it("Turn on modals", () => {
    cy.restoreLocalStorage();
    //turn off save on switch
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-modalDisplay > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();

    //check if saved settings changed
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("displayModals", true);
    });
    cy.saveLocalStorage();
  });

  it("Open node modal", () => {
    cy.restoreLocalStorage();

    cy.get("#dndnode_2_flow-Tab-4").dblclick();
    cy.get("#dndnode_2_flow-Tab-4-drawer-data").should("not.exist");
    cy.saveLocalStorage();
  });

  it("Turn of modals", () => {
    cy.restoreLocalStorage();
    //zavřít moda
    cy.get("button").contains("Cancel").click();
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-modalDisplay > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();

    //check if saved settings changed
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("displayModals", false);
    });
    cy.saveLocalStorage();
  });
});

describe("Connecting input to output node", () => {
  it("Connect", () => {
    //connecting two nodes
    cy.get("div[data-test-id='dndnode_1_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']")
      .trigger("mousemove")
      .trigger("mouseup");
  });
  //output node check
  it("Data transfer to output node check", () => {
    cy.get("#dndnode_2_flow-Tab-4").dblclick();
    cy.get("#dndnode_2_flow-Tab-4-drawer-data").click();
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div"
    ).should("have.text", "B");
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > div"
    ).should("have.text", "51");
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(3)"
    ).should("have.text", "driver");
    cy.get("#dndnode_2_flow-Tab-4-drawer-bar-chart").click();
    cy.get("#dndnode_2_flow-Tab-4-drawer-cancel-button").click();
  });
});

describe("Saving", () => {
  it("Saving", () => {
    cy.restoreLocalStorage();
    cy.get("button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Save logged", () => {
    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });

  it("Saved to localStorage", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content)
        .to.be.an("object")
        .that.has.property("nodes")
        .that.not.equal([]);
      expect(content).that.has.property("edges").that.not.equal([]);
    });
    cy.saveLocalStorage();
  });
});

describe("Input copy node, node status & connection status", () => {
  it("Open node menu", () => {
    cy.get("div").contains("Open tasks menu").click();
  });
  it("Start drag input copy node", () => {
    cy.get("#inputs-dropdown ul .dragable-node-input-copy").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
  });

  it("Drop on canvas", () => {
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
  });

  it("Input copy node state - initial", () => {
    // cy.log("-------Dot should be blue-------")
    cy.get("#dndnode_3_flow-Tab-4 .dot")
      .should("have.css", "background-color", "rgb(173, 216, 230)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Ready to work.");
    cy.get("#dndnode_3_flow-Tab-4 .dot").trigger("mouseleave");
    cy.wait(500);
  });

  it("Input copy node move", () => {
    cy.get("#dndnode_3_flow-Tab-4 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 500, clientY: 400 })
      .trigger("mouseup");
  });

  it("Output node have data from input preset node state - done", () => {
    // cy.log("-------Dot should be green-------")
    cy.get("#dndnode_2_flow-Tab-4 .dot")
      .should("have.css", "background-color", "rgb(0, 128, 0)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Done.");
    cy.get("#dndnode_2_flow-Tab-4 .dot").trigger("mouseleave");
    cy.wait(500);
  });
  it("Grab edge from input copy node - output handler", () => {
    cy.get("div[data-test-id='dndnode_3_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
  });

  it("Connection status - connection formats are compatible", () => {
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']")
      // vue-flow__handle vue-flow__handle-left vue-flow__handle-a nodrag target connectable
      //acceptance conection color check
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(173, 255, 47)");
  });

  it("Drop edge line on console output - input handler (finish connecting)", () => {
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']").trigger(
      "mouseup"
    );
  });

  // cy.log("-------Dot should be red-------")

  // it('Console output status check (error - no data from newly connected input copy node)', () => {
  it("Console output status check (done - no data from newly connected input copy node -> data source is still preset input node)", () => {
    cy.get("#dndnode_2_flow-Tab-4 .dot")
      .should("have.css", "background-color", "rgb(0, 128, 0)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Done.");
    cy.get("#dndnode_2_flow-Tab-4 .dot").trigger("mouseleave");
    cy.wait(500);
  });

  it("Wrong input connection", () => {
    //error connection color check
    cy.get("div[data-test-id='dndnode_3_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-b-input']")
      // vue-flow__handle vue-flow__handle-left vue-flow__handle-a nodrag target connectable
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(255, 0, 0)");
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-b-input']").trigger(
      "mouseup"
    );
  });
});

describe("Auto data propagation", () => {
  it("Get data for input copy node", () => {
    cy.get("#dndnode_3_flow-Tab-4 button").contains("Parse").click();
    // cy.log("-------Dot should be green-------")
    cy.get("#dndnode_3_flow-Tab-4 .dot")
      .should("have.css", "background-color", "rgb(0, 128, 0)")
      .trigger("mouseenter");
    cy.get(".ant-tooltip-inner:visible").should("have.text", "Done.");
    cy.get("#dndnode_3_flow-Tab-4 .dot").trigger("mouseleave");
    cy.wait(500);
  });
  it("Check data auto transfer to console log output node", () => {
    //output node check
    cy.get("#dndnode_2_flow-Tab-4").dblclick();
    cy.get("#dndnode_2_flow-Tab-4-drawer-data").click();
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1) > div"
    ).should("have.text", "A");
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > div"
    ).should("have.text", "27");
    cy.get(
      "#dndnode_2_flow-Tab-4-drawer-data > ul > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(3)"
    ).should("have.text", "Doctor");
    cy.get("#dndnode_2_flow-Tab-4-drawer-cancel-button").click();
  });
});

describe("Create cleaning node", () => {
  it("Open prep tasks menu", () => {
    //add prepare task
    cy.get("#prep-dropdown").click();
  });

  it("Start dragging cleaning node", () => {
    cy.get("#prep-dropdown ul .dragable-node-prep-clean").trigger("dragstart", {
      dataTransfer,
    });
  });

  it("Drop cleaning node to canvas", () => {
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
  });

  it("Move cleaning node", () => {
    cy.get("#dndnode_4_flow-Tab-4 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1000, clientY: 100 })
      .trigger("mouseup");
  });
});

describe("Connect cleaning node", () => {
  it("Connect input copy node to cleaning node", () => {
    //connect preapre task
    cy.get("div[data-test-id='dndnode_3_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_4_flow-Tab-4-a-input']")
      // vue-flow__handle vue-flow__handle-left vue-flow__handle-a nodrag target connectable
      //acceptance conection color check
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(173, 255, 47)");
    cy.get("div[data-test-id='dndnode_4_flow-Tab-4-a-input']").trigger(
      "mouseup"
    );
  });

  it("Connect cleaning node to output", () => {
    //connect to output node
    cy.get("div[data-test-id='dndnode_4_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']")
      // vue-flow__handle vue-flow__handle-left vue-flow__handle-a nodrag target connectable
      //acceptance conection color check
      .trigger("mousemove")
      .should("have.css", "background-color", "rgb(173, 255, 47)");
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-a-input']").trigger(
      "mouseup"
    );
  });
});

describe("Cleaning process check - footer output/input - auto action", () => {
  it("Original data", () => {
    //test footer panel data
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1) > div"
    ).should("have.text", "John");
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div"
    ).should("have.text", "Zdenek");
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(1) > div"
    ).should("have.text", "");
  });

  it("Cleaned data", () => {
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1) > div"
    ).should("have.text", "John");
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div"
    ).should("have.text", "Zdenek");
    cy.get(
      "#data-preview-outputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(1) > div"
    ).should("not.exist");
  });
});

describe("Flow position", () => {
  it("Moving with flow (background)", () => {
    cy.restoreLocalStorage();
    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });
    cy.get("button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Scroll flow (background)", () => {
    cy.restoreLocalStorage();
    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });
    cy.get("button span").contains("Save").click();
    cy.saveLocalStorage();
  });

  it("Moved and zoomed", () => {
    cy.restoreLocalStorage();
    cy.getLocalStorage("flow-Tab 4").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object").that.has.property("nodes");
      expect(content).that.has.property("edges");
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2)
        .to.have.members([250, 373.5]);
      expect(content).that.has.property("zoom").that.equal(0.5);
    });
    cy.saveLocalStorage();
  });
});

describe("Multiple inputs", () => {
  it("Create nodes", () => {
    cy.get("#inputs-dropdown ul .dragable-node-input-server-set").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );
    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });
    cy.get("#dndnode_5_flow-Tab-4 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 200, clientY: 500 })
      .trigger("mouseup");
  });

  it("Data set up", () => {
    cy.get("#dndnode_5_flow-Tab-4server-input-node").click({ force: true });
    cy.get(".ant-select-item-option-content")
      .contains("62874bb8572dcc6ae6ce66d9")
      .click();
    cy.get("#dndnode_5_flow-Tab-4 button").contains("Select dataset").click();
    cy.get("#dndnode_5_flow-Tab-4 .dot").should(
      "have.css",
      "background-color",
      "rgb(0, 128, 0)"
    );
  });

  it("Connection to log output node ", () => {
    cy.get("div[data-test-id='dndnode_5_flow-Tab-4-a-output']").trigger(
      "mousedown"
    );
    cy.get("div[data-test-id='dndnode_2_flow-Tab-4-b-input']")
      .trigger("mousemove")
      .trigger("mouseup");
  });

  it("Check data trasnfer table", () => {
    //test footer panel data
    cy.get("#dndnode_2_flow-Tab-4").click();
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1) > div"
    ).should("have.text", "John");
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div"
    ).should("have.text", "Zdenek");
    cy.get(
      "#data-preview-inputs > div.content-container > div > div > div > div > div > div > div > table > tbody > tr:nth-child(3)"
    ).should("not.exist");
  });
  it("Check data trasnfer string", () => {
    cy.get("#data-preview-inputs-select-input").click({ force: true });
    cy.get(".ant-select-item-option-content").contains("data_string").click();
    cy.get("#data-preview-inputs > div.content-container").should(
      "have.text",
      "God is Great! I won a lottery."
    );
  });
});

describe("Edge changes - auto changing source", () => {
  it("Edge options button", () => {
    cy.get(
      "#dndnode_1_flow-Tab-4-dndnode_2_flow-Tab-4.buttonRow .ant-btn.ant-btn-primary.ant-btn-sm.edgebutton"
    ).click();
    cy.get("#dndnode_1_flow-Tab-4-dndnode_2_flow-Tab-4-close-drawer").click({
      force: true,
    });
  });

  it("Active source edge", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();

    cy.get("#log ul li:last-child").should(
      "have.text",
      "God is Great! I won a lottery."
    );
  });

  it("Change active edge to from different input type", () => {
    cy.get("#dndnode_4_flow-Tab-4-dndnode_2_flow-Tab-4.buttonRow button")
      .contains("Run")
      .click();
  });

  it("Active source edge - different input type", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();
    cy.get("#log ul li:last-child").should(
      "have.text",
      '[{"name":"John","age":"35","job":"assasin"},{"name":"Zdenek","age":"26","job":"developer"}]'
    );
  });

  it("Change active edge from same input type", () => {
    cy.get("#dndnode_3_flow-Tab-4-dndnode_2_flow-Tab-4.buttonRow button")
      .contains("Run")
      .click();
  });

  it("Active source edge - same input type", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();
    cy.get("#log ul li:last-child").should(
      "have.text",
      '[{"name":"John","age":"35","job":"assasin"},{"name":"Zdenek","age":"26","job":"developer"},{"name":"","age":"34","job":"cook"}]'
    );
  });

  it("Remove active edge", () => {
    cy.get(
      "#dndnode_3_flow-Tab-4-dndnode_2_flow-Tab-4.buttonRow .anticon.anticon-close"
    ).click();
  });

  it("Active source edge - removed active edge", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();
    cy.get("#log ul li:last-child").should(
      "have.text",
      '[{"name":"John","age":"35","job":"assasin"},{"name":"Zdenek","age":"26","job":"developer"}]'
    );
  });
});

describe("Source node removed", () => {
  it("Source data check", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();
    cy.get("#log ul li:last-child").should(
      "have.text",
      '[{"name":"John","age":"35","job":"assasin"},{"name":"Zdenek","age":"26","job":"developer"}]'
    );
  });

  it("Remove source node", () => {
    cy.get("#dndnode_1_flow-Tab-4 button span.anticon.anticon-close").click();
  });

  it("New source data check", () => {
    cy.get(
      "#dndnode_2_flow-Tab-4 button span.anticon.anticon-caret-right"
    ).click();
    cy.get("#dndnode_2_flow-Tab-4 button span").contains("CONSOLE_LOG").click();
    cy.get("#log ul li:last-child").should(
      "have.text",
      "God is Great! I won a lottery."
    );
  });
});

describe("Language change", () => {
  it("Language is set to english", () => {
    //change language
    cy.get("#log > div.header > h3").should("have.text", "Log:");
  });

  it("Language switch", () => {
    cy.get(".ant-switch .ant-switch-inner").contains("CS").click();
  });

  it("Check language changed to czech", () => {
    cy.get("#log > div.header > h3").should("have.text", "Historie:");
  });

  it("Language switch back", () => {
    cy.get(".ant-switch .ant-switch-inner").contains("EN").click();
  });

  it("Check language changed back to english", () => {
    cy.get("#log > div.header > h3").should("have.text", "Log:");
  });
});

describe("Accept modal", () => {
  it("Cancel", () => {
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#menu-language-switch > button").click();

    // cy.get(".drawer-header .drawer-header button span.anticon.anticon-close").click()
  });
  it("Cancel", () => {
    cy.get(".ant-modal-content button").contains("Cancel").click();
    cy.get(".ant-drawer-body h1").should("have.text", "Settings");
  });
  it("Trigger accept modal", () => {
    cy.get("#menu-language-switch > button").click();
  });
  it("Ok", () => {
    cy.get(".ant-modal-content button").contains("Ok").click();
    cy.get(".ant-drawer-body h1").should("have.text", "Nastavení");
  });

  it("Disable accept modal", () => {
    cy.get("#settings-acceptModalAllowed > button").click();
  });

  it("Try to trigger accept modal", () => {
    cy.get("#menu-language-switch > button").click();
  });

  it("Accept modal exist?", () => {
    cy.get(".ant-modal-mask").should("have.css", "display", "none");

    cy.get(".drawer-header button span.anticon.anticon-close").click();
  });
});

describe("Keyboard events", () => {
  it("ctrl + s", () => {
    cy.get("body").type("{ctrl}s");

    cy.get("#log ul li:last-child").should("have.text", "Saved!");
  });
  it("The rest of tests", () => {});
});

describe("Dodělat", () => {
  //dodělat test na drawery
  //dodělat test na historii
  //dodělat test na přepínání settings záložky
  //
  //~~dodělat mazání uzlů~~
  //~~dodělat mazání hran~~
  //~~dodělat automatické přepínání vstupů při vymazání~~
  //~~dodělat modal místo draweru~~
  //~~dodělat počet tabů~~
  //~~dodělat zobrazení akčních tlačítek~~
  //~~dodělat test velikosti handlu~~
  //~~dodělat v aplikaci acceptmodal on tab close~~
  //~~dodělat v aplikaci nezobrazit sekci akce v draweru(modalu), když žádné akce nejsou dostupné~~
  //dodělat v aplikaci akci výběr mezi dalšími možnostmi s neuplnými daty - na akci výběr se otevře mdodalové okno, které zvolí nabídku
  //~~dodělat v aplikaci animated edge opravit~~
  //dodělat v aplikaci vypínatelný autorun akcí
  //bug? console output s hranami někdy zůstává zobrazený i když změním tab, ale po refreshi se to fixne
  //~~dodělat v aplikaci nastavitelná velikost handlů~~¨
  //dodělat opravu přepínání mezi taby - nemění se načtený flow
  it("Switching between flows auto saving", () => {
    cy.restoreLocalStorage();
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 3']"
    ).click();
    cy.get("div").contains("Open tasks menu").click();
    // cy.get("div").contains("Otevřít nabídku úloh").click()

    cy.get("#inputs-dropdown ul .dragable-node-input-preset").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );

    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });

    cy.get("#dndnode_1_flow-Tab-3 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1000, clientY: 100 })
      .trigger("mouseup");

    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });

    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });

    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 2']"
    ).click();

    cy.getLocalStorage("flow-Tab 3").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("nodes").that.has.length(1);
      expect(content).that.has.property("edges").that.has.length(0);
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2);
      expect(content.position[0]).equal(-210);
      expect(content.position[1]).equal(463.5);
      expect(content).that.has.property("zoom", 0.5);
    });

    cy.wait(500);

    cy.saveLocalStorage();
  });

  it("Switching between tabs turned off", () => {
    cy.restoreLocalStorage();
    //turn off save on switch
    cy.get("button span.anticon.anticon-menu").click();
    cy.get("#settings-autoTabSave > button").click();
    cy.get(".drawer-header button span.anticon.anticon-close").click();

    //check if saved settings changed
    cy.getLocalStorage("settings").then((settings) => {
      const content = JSON.parse(settings);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("autoSaveOnSwitch", false);
    });
    //switch on tab 2
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 2']"
    ).click();
    cy.get("div").contains("Open tasks menu").click();
    // cy.get("div").contains("Otevřít nabídku úloh").click()

    //add node
    cy.get("#inputs-dropdown ul .dragable-node-input-preset").trigger(
      "dragstart",
      {
        dataTransfer,
      }
    );

    cy.get(".vue-flow").trigger("drop", {
      dataTransfer,
    });

    //move node
    cy.get("#dndnode_1_flow-Tab-2 .anticon.anticon-holder.svgHolder")
      .trigger("mousedown", "center")
      .trigger("mousemove", { clientX: 1000, clientY: 100 })
      .trigger("mouseup");

    //zoom
    cy.get(".vue-flow__pane").trigger("wheel", {
      deltaY: 666.666666,
      bubbles: true,
    });

    //move with flow
    cy.window().then((win) => {
      cy.get(".vue-flow__pane")
        // use force and pass win as view to event
        .trigger("mousedown", "center", { force: true, view: win })
        .trigger("mousemove", {
          force: true,
          clientX: 500,
          clientY: 550,
          view: win,
        })
        .trigger("mouseup", { force: true, view: win });
    });

    //switch to tab 1
    cy.get(
      ".ant-menu-overflow-item.ant-menu-item[data-menu-id='Tab 1']"
    ).click();

    //check if tab 2 is not saved
    cy.getLocalStorage("flow-Tab 2").then((tab) => {
      const content = JSON.parse(tab);
      expect(content).to.not.undefined;
      expect(content).to.be.an("object");
      expect(content).that.has.property("nodes").that.has.length(0);
      expect(content).that.has.property("edges").that.has.length(0);
      expect(content)
        .that.has.property("position")
        .that.is.an("array")
        .that.has.length(2);
      expect(content.position[0]).equal(0);
      expect(content.position[1]).equal(0);
      expect(content).that.has.property("zoom", 1);
    });

    cy.saveLocalStorage();
  });
});
