import { PRE_PRE_PROCESS } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV, DATA_TOKENS, DATA_TEXT_DATASET } from "../../constants/dataTypes";
import { SERVER_REQUEST_PRE_PROCESS  } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { getApi, postApi } from "../../services/server"
import * as url from "../../constants/url"
import { translate } from "../../languages/languages"
import { useStore } from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"


export class PreProcess {
    id: string
    nodeType: string = PRE_PRE_PROCESS
    data: string[]
    dataType: string
    dataColumns: string[]
    selectedColumns: string[]
    flowNodeId: string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    status: string
    statusMessage: string
    serverData: {}
    errorHandlingType: string
    language: string
    targetColumn:string
    inplaceColumn: boolean
    newColumn:string
    columnsToDrop: string[]
    stop: boolean
    
    removeStopwords: boolean
    useCustomStopwords: boolean
    customStopwords: string[]
    lemmatize: boolean
    toLowercase: boolean
    removeNumbers: boolean
    removeWordsContainingNumbers: boolean
    removeEmoticons: boolean
    removeAccents: boolean
    trimWhitespaces: boolean
    tokenizeResult: boolean


    constructor(flowNodeId: string) {
        this.id = "Logic-" + flowNodeId
        this.data = []
        this.dataType = DATA_TEXT_DATASET
        this.dataColumns = []
        this.selectedColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [
            {
                source: "input",
                type: DATA_TEXT_DATASET,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ]
        this.outputs = [
            {
                name: "Raw data",
                type: DATA_TEXT_DATASET,
                next: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ],
            this.actions = [
                {
                    type: SERVER_REQUEST_PRE_PROCESS,
                    callback: () => this.serverPreProcess(),
                    description: "Run cleaning process.",
                    autoRun: true,
                    icon: "SettingOutlined"
                },
                // {
                //     type:SELECT_CLEAN_METHOD,
                //     callback: () => this.selectCleanMethodCallback(),
                //     description: "Select type of cleaning process.",
                //     autoRun: false,
                //     icon: "SettingOutlined"
                // }
            ]
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.serverData = {}
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE,
        this.language = "english" 

        this.targetColumn = "text"
        this.inplaceColumn = true
        this.newColumn = "text"
        this.columnsToDrop = []
        this.stop = false

        
        this.removeStopwords = false
        this.useCustomStopwords = false
        this.customStopwords = []
        this.lemmatize = false
        this.toLowercase = false
        this.removeNumbers = false
        this.removeWordsContainingNumbers = false
        this.removeEmoticons = false
        this.removeAccents = false
        this.trimWhitespaces = false
        this.tokenizeResult = false

    }

    clone = (id = this.flowNodeId) => {
        let clone = new PreProcess(id)

//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage
        clone.language = this.language
        clone.errorHandlingType = this.errorHandlingType      
        
        clone.targetColumn = this.targetColumn
        clone.inplaceColumn = this.inplaceColumn
        clone.newColumn = this.newColumn
        clone.columnsToDrop = this.columnsToDrop
        
        clone.removeStopwords = this.removeStopwords
        clone.useCustomStopwords = this.useCustomStopwords
        clone.customStopwords = this.customStopwords
        clone.lemmatize = this.lemmatize
        clone.toLowercase = this.toLowercase
        clone.removeNumbers = this.removeNumbers
        clone.removeWordsContainingNumbers = this.removeWordsContainingNumbers
        clone.removeEmoticons = this.removeEmoticons
        clone.removeAccents = this.removeAccents
        clone.trimWhitespaces = this.trimWhitespaces
        clone.tokenizeResult = this.tokenizeResult



        return clone
    }

    changeLanugage = (lang) => {
        //console.log(lang)
        this.language = lang //[english|czech]
    }
//changeErrorHandlingType zakomponovat do customNodes.vue
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    changeTargetColumn = (targetColumn) => {
        //console.log(targetColumn)
        this.targetColumn = targetColumn //string
    }

    changeInplaceColumn = (inplaceColumn) => {
        //console.log(inplaceColumn)
        this.inplaceColumn = inplaceColumn //[true|false]
    }

    changeNewColumn = (newColumn) => {
        //console.log(newColumn)
        this.newColumn = newColumn //string
    }

    changeColumnsToDrop = (columnsToDrop) => {
        //console.log(columnsToDrop)
        this.columnsToDrop = columnsToDrop //string[]
    }

    //-------------------------------

    changeRemoveStopwords = (removeStopwords) => {
        //console.log(removeStopwords)
        this.removeStopwords = removeStopwords //boolean
    }

    changeUseCustomStopwords = (useCustomStopwords) => {
        //console.log(useCustomStopwords)
        this.useCustomStopwords = useCustomStopwords //boolean
    }

    changeCustomStopWords = (customStopwords) => {
        //console.log(customStopwords)
        this.customStopwords = customStopwords //string[]
    }

    changeLemmatize = (lemmatize) => {
        //console.log(lemmatize)
        this.lemmatize = lemmatize //boolean
    }

    changeToLowerCase = (toLowercase) => {
        //console.log(toLowercase)
        this.toLowercase = toLowercase //boolean
    }

    changeRemoveNumbers = (removeNumbers) => {
        //console.log(removeNumbers)
        this.removeNumbers = removeNumbers //boolean
    }

    changeRemoveWordsContainingNumbers = (removeWordsContainingNumbers) => {
        //console.log(removeWordsContainingNumbers)
        this.removeWordsContainingNumbers = removeWordsContainingNumbers //boolean
    }

    changeRemoveEmoticons = (removeEmoticons) => {
        //console.log(removeEmoticons)
        this.removeEmoticons = removeEmoticons //boolean
    }

    changeRemoveAccents = (removeAccents) => {
        //console.log(removeAccents)
        this.removeAccents = removeAccents //boolean
    }

    changeTrimWhitespaces = (trimWhitespaces) => {
        //console.log(trimWhitespaces)
        this.trimWhitespaces = trimWhitespaces //boolean
    }

    changeTokenizeResult = (tokenizeResult) => {
        //console.log(tokenizeResult)
        this.tokenizeResult = tokenizeResult //boolean
    }


    tryToFix = () => {
        //console.log("under construction...")
    }

    canBeActionCalled = () => {
        //console.log(this.errorHandlingType)
        
        if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_IGNORE){
            //console.log("ERROR_HANDLING_IGNORE")
            return true
        }

        const typeOfData = typeof this.data.payload[0].text
        //console.log(typeOfData)
        
        let error = false
        let errorMessage = ""

        if (typeOfData !== "object"){
            error = true
            errorMessage = "status.datasetNotObject"
        }

        if (error){
            if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_TRY_TO_FIX){
                this.changeStatus(statusTypes.STATUS_ERROR, errorMessage)
                
                //console.log("ERROR_HANDLING_TRY_TO_FIX")
                this.tryToFix()
                return false
            }
            
            // if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_STOP){
                //console.log("ERROR_HANDLING_STOP")
                
                this.changeStatus(statusTypes.STATUS_ERROR, errorMessage)
                return false
            // }

        }
        //console.log("NON-ERROR-NON-IGNORE")

        return true

    }

    serverPreProcess = async () => {

        if (this.canBeActionCalled()){
            
            
            let dataId = ""
            for (let input of this.inputs){
                if (input.type === DATA_TEXT_DATASET){
                    dataId = input.data._id
                }
            }

            const body = {
                "workspace_id": useStore().activeFlowId,
                "user_id": useStore().activeUser.token,
                "input_data_id": dataId,
                // "input_data_id": this.data._id,
                "task": "preprocess_text",
                "lang": this.language,
                "target_column": this.targetColumn,
                "inplace_column": this.inplaceColumn,
                "new_column_name": this.newColumn,
                "columns_to_drop": this.columnsToDrop,
                "remove_stopwords": this.removeStopwords,
                "use_custom_stopwords": this.useCustomStopwords,
                "custom_stopwords" : this.customStopwords,
                "lemmatize": this.lemmatize,
                "to_lowercase": this.toLowercase,
                "remove_numbers": this.removeNumbers,
                "remove_words_containing_numbers": this.removeWordsContainingNumbers,
                "remove_emoticons": this.removeEmoticons,
                "remove_accents": this.removeAccents,
                "trim_whitespaces": this.trimWhitespaces,
                "tokenize_result": this.tokenizeResult,
                // remove_stopwords: whether or not to remove stopwords
                // use_custom_stopwords: whether or not to use custom stopwords set defined by user, passed only if remove_stopwords is True
                // custom_stopwords: list of words to filter eg. ['the', 'a', 'an'], passed only if use_custom_stopwords is True
                // lemmatize: whether or not to lemmatize input text
                // to_lowercase: whether or not to transform the text into lowercase
                // remove_numbers: whether or not to remove numbers from text
                // remove_words_containing_numbers: whether or not to remove words containing numbers from text
                // remove_emoticons: whether or not to remove emoticons from text
                // remove_accents: whether or not to remove accents from text
                // trim_whitespaces: whether or not to remove any reduntant whitespaces
                // tokenize_result: whether or not to tokenize the result

                // "target_column": "text",
                // "inplace_column": "True",
                // "new_column_name": "text",
                // "columns_to_drop": [
                //     "text"
                // ]
            }

            //console.log(this)
            let response = await postApi(url.URL_LIST_OF_TASKS, body)
            //console.log(response)
            if (response.status !== 422) {

                this.status = statusTypes.STATUS_RUNNING

                this.statusMessage = "status.requestServer"
                useStore().flowFunctions.refreshNodeWithSameLogicNode(this.flowNodeId, this)
                this.serverData = response.data
                const { status, statusMessage } = await this.checkStatus()
                this.status = status
                // //console.log(status, statusMessage)

                this.statusMessage = statusMessage
            } else {
                this.statusMessage = "status.e422"
                this.status = statusTypes.STATUS_ERROR


            }
            // //console.log(this)
            let timeOut = setTimeout(() => {

                useStore().flowFunctions.refreshNodeWithSameLogicNode(this.flowNodeId, this)
            }, 1000)
        }
    }

    checkStatus = async () => {
        let response
        let self = this
        return await new Promise(function (resolve, reject) {
            let interval = setInterval(async () => {
                response = await getApi(url.URL_LIST_OF_TASKS, self.serverData._id).then(async(response) => {
                    //console.log(response)
                    if (response.status !== "started"){

                        switch (response.status) {
                            case "Dataset not found":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.datasetNotFound"
                                })
                                break;
                            case "Task failed: Target column is not present in dataset.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.columNoInData"
                                })
                                break;
                            case "Task failed: Target column values must be a string.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.targetColumnMustBeString"
                                })
                                break;
                            case "Task failed: Target column values must be a list.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.targetColumnMustBeList"
                                })
                                break;
                            case "succeeded":
                                clearInterval(interval)

                                //z odpovědi mám id na vzniklý dataset je potřeba zavolat dataset od serveru a ten uložit jako data 
                                //- vzniknou tak unifikovaný data pro všechny NLP uzly
                                // ze serveru ale nedostávám id nově vzniklého datasetu??
                                //console.log(response)
                                let newD = await getApi(url.URL_SPECIFIC_DATASET(response.output_data_id))

                                //console.log(newD)
                                    resolve({
                                        status: statusTypes.STATUS_DONE,
                                        statusMessage: "status.succeeded"
                                    })
                                    self.data = newD
                                    for (let output of self.outputs) {
                                        if (output.type === DATA_TEXT_DATASET) {
                                            output.data = newD
                                            output.progress = 100
                                        }
                                    }
                            
                                break;
                        
                            default:
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.unknownError"
                                })
                                break;
                        }
                    }
                })
                if (self.stop){
                    
                    clearInterval(interval)
                    resolve({
                        status: statusTypes.STATUS_ERROR,
                        statusMessage: "status.responseTooLong"
                    })
                }
            }, 1000)
        });
    }

    stopProcess = () => {
        this.stop = true

        setTimeout(()=>{
            this.stop = false
        },3000)
    }

    changeStatus = (status: string, statusMessage: string) => {
        this.status = status
        this.statusMessage = statusMessage
    }

    resetInput() {
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ]
    }
    onInput(inputType, data, dataColumns) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }


    getDataFromInput(inputType: string) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                this.data = i.data
                this.dataColumns = i.dataColumns
            }
        }
    }

} 