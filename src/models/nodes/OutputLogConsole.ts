import { OUTPUT_LOG_CONSOLE } from "../../constants/nodeTypes";
import { DATA_TOKENS, DATA_CSV, DATA_TEXT_DATASET, DATA_TABLE } from "../../constants/dataTypes";
import { CONSOLE_LOG } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { translate } from "../../languages/languages"
import { useStore } from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"
import { getReadableTokens } from "../../helpers/globalHelper";

// import {useStore} from "../../stores/globalStore"
// const store = useStore()
export class OutputLogConsole {
    id: string
    nodeType: string = OUTPUT_LOG_CONSOLE
    data: {}[]
    dataType: string
    dataColumns: string[]
    flowNodeId: string
    outputs: {}[]
    inputs: {}[]
    status: string
    statusMessage: string
    actions: {}[]
    errorHandlingType: string

    constructor(flowNodeId: string) {
        this.id = "Logic-" + flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [

            {
                source: "input",
                type: DATA_TOKENS,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            },
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            },
            {
                source: "input",
                type: DATA_TEXT_DATASET,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            },
            // {
            //     source: "input",
            //     type: DATA_CSV,
            //     previous: [],
            //     data: "",
            //     progress: 0
            // }
        ]
        this.outputs = []
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.actions = [
            {
                type: CONSOLE_LOG,
                callback: () => this.output(),
                autoRun: true
            },
        ]
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }


    onInput(inputType, data, dataColumns) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }

    changeStatus = (status: string, statusMessage: string) => {
        this.status = status
        this.statusMessage = statusMessage
    }

    getDataFromInput(inputType: string) {
        let someInputHaveActiveSource = false
        for (let i of this.inputs) {
            if (i.type === inputType) {



                this.data = i.data
                this.dataType = inputType
                this.dataColumns = i.dataColumns
            }
        }

        for (let i of this.inputs) {
            if (i.data.length !== 0) {
                someInputHaveActiveSource = true
            }
        }
        if (someInputHaveActiveSource) {
            this.status = statusTypes.STATUS_DONE,
                this.statusMessage = "status.done"
        } else {
            this.status = statusTypes.STATUS_ERROR,
                this.statusMessage = "status.noDataInput"
        }
    }

    clone = (id = this.flowNodeId) => {
        let clone = new OutputLogConsole(id)

//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage




        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    output() {
        //console.log(this.dataType)
        if (this.dataType === DATA_TOKENS) {
            useStore().addLog({
                message: getReadableTokens(this.data),
                formated: true
            })
        } else if (this.data.text) {
            useStore().addLog(this.data.text)
        } else {
            useStore().addLog({
                message: JSON.stringify(this.data),
                formated: false
            })
        }
    }
} 