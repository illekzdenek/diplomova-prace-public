import { PRE_SELECT } from "../../constants/nodeTypes";
import { DATA_TABLE } from "../../constants/dataTypes";
import { SELECT_COLUMNS } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { translate } from "../../languages/languages"
import {useStore} from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"

export class PreSelect {
    id: string
    nodeType: string = PRE_SELECT
    data: {}[]
    dataType: string
    dataColumns: string[]
    selectedColumns: string[]
    flowNodeId:string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    status: string
    statusMessage: string
    errorHandlingType: string

    constructor(flowNodeId:string){
        this.id = "Logic-"+flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.selectedColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns:[],
                progress: 0
            },
            // {
            //     source: "input",
            //     type: DATA_CSV,
            //     previous: [],
            //     data: "",
            //     progress: 0
            // },
        ]
        this.outputs = [
            {
                name:"Raw data",
                type: DATA_TABLE,
                next: [],
                data: [],
                dataColumns:[],
                progress: 0
            }
            // ,{
            //     name:"Raw data",
            //     type: DATA_TABLE,
            //     columns: this.selectedColumns,
            //     next: [],
            //     data: "",
            //     progress: 0
            // },
        ],
        this.actions = [
            {
                type:SELECT_COLUMNS,
                callback:() => this.selectColumns(),
                description: "Select type of cleaning process.",
                autoRun: true,
                icon: "SettingOutlined"
            }
        ]
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }
    
    clone = (id=this.flowNodeId) => {
        let clone = new PreSelect(id)
        
//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage



        clone.selectedColumns = this.selectedColumns

        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    selectColumns (selectedColumns:string[]=this.selectedColumns) {
       
        let input
        for (let i of this.inputs){
            if (i.type === DATA_TABLE){
                input = i
            }
        }
        let newData = []
        let newOutputs = []
        for (let output of this.outputs){
            let newOutput = output
            if (output.type === DATA_TABLE){
                newOutput.dataColumns = selectedColumns
                if (selectedColumns.length > 0){
                    for(let idata of input.data){
                        let dataObject = {}
                        for(let property in idata){
                            if(selectedColumns.indexOf(property)!==-1){
                                
                                dataObject[property]=idata[property]
                            }
                        }
                        
                        newData.push(dataObject)
                    }
                    newOutput.data = newData
                    newOutput.progress = 100
                } else {
                    newOutput.data = []
                    newOutput.progress = 0
                }
            }
            newOutputs.push(newOutput)
        }
        
        this.outputs = newOutputs
        this.selectedColumns = selectedColumns

        if (newOutputs[0].dataColumns.length>0){
            this.status = statusTypes.STATUS_DONE,
            this.statusMessage = "status.done"
            // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
        } else {
            this.status = statusTypes.STATUS_RUNNING,
            this.statusMessage = "status.waitingForFilter"
            // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
        }
        if (this.data.length === 0) {
            this.status = statusTypes.STATUS_ERROR,
            this.statusMessage = "status.noDataInput"
        } 
    }

    getDataFromInput (inputType:string){
        for (let i of this.inputs){
            if (i.type === inputType){
                this.data = i.data
                this.dataColumns = i.dataColumns
                this.status = statusTypes.STATUS_RUNNING,
                this.statusMessage = "status.waitingForFilter"
            }
        }
    }

    changeStatus = (status:string, statusMessage:string) => {
        this.status=status
        this.statusMessage = statusMessage
    }

    onInput (inputType, data, dataColumns){
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        for (let i of this.inputs){
            if (i.type === inputType){
                i.data = data
                i.dataColumns = dataColumns
            }
        }
        // for (let o of this.outputs){
        //     if (o.type === inputType){
        //         o.data = []
        //         o.dataColumns = []
        //     }
        // }
    }

} 