import { INPUT_PRESET } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV } from "../../constants/dataTypes";
import * as statusTypes from "../../constants/statusTypes"
import {ADD_DATA} from "../../constants/actionTypes"
import { translate } from "../../languages/languages"
import {useStore} from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"

export class InputPreset {
    id: string
    nodeType: string = INPUT_PRESET
    data: {}[]
    dataType: string
    dataColumns: string[]
    flowNodeId:string
    outputs: {}[]
    actions: {}[]
    inputs: []
    status: string
    statusMessage: string
    errorHandlingType: string
    

    constructor(flowNodeId:string){
        this.id = "Logic-"+flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = []
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.outputs = [
            {
                type: DATA_TABLE,
                next: [],
                data: [],
                dataColumns:[],
                progress: 0
            },
            // {
            //     type: DATA_CSV,
            //     next: [],
            //     data: "",
            //     progress: 0
            // }
        ],
        this.actions = [
            {
                type:ADD_DATA,
                callback:() => this.sendMockdataTable(),
                description: "Set mockup datat to this node.",
                autoRun: true,
                icon: "SettingOutlined"
            }
        ]
        // this.getData()
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }

    getData = () => {
        
        return {
            data: [
                {
                    name: "A",
                    age: 21,
                    job: ""
                },
                {
                    name: "B",
                    age: 27,
                    job: "Lawyer"
                },
                {
                    name: "C",
                    age: 51,
                    job: "Doctor"
                },
                {
                    name: "D",
                    age: 45,
                    job: "driver"
                },
            ],
            dataColumns: ["name", "age", "job"]
        }
    }

    sendDataToNext = (outputType, data, progress) => {
        //get from api
        //wait for response
        //set results from response to handle
        for (let output of this.outputs) {
            if (output.type === outputType){
                output.data = data,
                output.progress = progress
                output.dataColumns = this.dataColumns
                // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
            }
        }
    }

    sendMockdata = () => {
        for (let i = 0; i < mockProgress.length; i++) {
                
                this.sendDataToNext(mockProgress[i].outputType, mockProgress[i].data, mockProgress[i].progress)
        }
        
    }
    
    sendMockdataTable = () => {
        // for (let [index,data] of this.data.entries()){
        //     const progress = ((index+1)/this.data.length)*100
        //     let d = this.data
        //     d.push(data)
        let clone = this.clone()
        
        let d = clone.getData()
        clone.data = d.data
        clone.dataColumns = d.dataColumns
        

        for (let output of clone.outputs){
            
            if (output.type === DATA_TABLE){
                output.data = d.data
                output.progress = 100
                output.dataColumns = d.dataColumns
            }
        }
        clone.status = statusTypes.STATUS_DONE
        // clone.statusMessage =  translate(useStore().language,"status.done")
        clone.statusMessage =  "status.done"
        return clone
            // this.sendDataToNext(DATA_TABLE,this.data,100)
        // }
        // for (let i = 0; i < mockProgressTable.length; i++) {
        //         this.sendDataToNext(mockProgressTable[i].outputType, mockProgressTable[i].data, mockProgressTable[i].progress)
        // }
        
    }

    changeStatus = (status:string, statusMessage:string) => {
        this.status=status
        this.statusMessage = statusMessage
    }
//uchovávat jenom klíče k překladům 

    clone = (id=this.flowNodeId) => {
        let clone = new InputPreset(id)
        
        
        clone.id = "Logic-"+id
//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage




        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }
} 

let counterTable = 0
const mockProgressTable = [
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 10
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 20
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 30
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 40
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 50
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 60
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 70
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 80
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 90
    },
    {
        outputType: DATA_TABLE,
        data: [
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
            {
                name: "John",
                age: 26,
                job: "developer"
            },
        ],
        progress: 100
    },
]

let counter = 0
const mockProgress = [
    {
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer",
        progress: 10
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer",
        progress: 20
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 30
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 40
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 50
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 60
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 70
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 80
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 90
    },{
        outputType: DATA_CSV,
        data: "NAME;AGE;JOB;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer;\n;John;25;Developer",
        progress: 100
    },
]