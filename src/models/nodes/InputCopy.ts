import { INPUT_COPY } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV } from "../../constants/dataTypes";
import * as statusTypes from "../../constants/statusTypes"
import { translate } from "../../languages/languages"
import {useStore} from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"

export class InputCopy {
    id: string
    nodeType: string = INPUT_COPY
    data: {}[]
    dataType: string
    dataColumns: string[]
    flowNodeId:string
    outputs: {}[]
    inputs: []
    status: string
    statusMessage: string
    errorHandlingType: string

    constructor(flowNodeId:string){
        this.id = "Logic-"+flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = []
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.outputs = [
            {
                type: DATA_TABLE,
                next: [],
                data: [],
                dataColumns:[],
                progress: 0
            }
        ]
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }

    sendDataToNext = (outputType, data, progress) => {
        //get from api
        //wait for response
        //set results from response to handle
        for (let output of this.outputs) {
            if (output.type === outputType){
                output.data = data,
                output.progress = progress
                output.dataColumns = this.dataColumns
                this.changeStatus(statusTypes.STATUS_DONE, "status.done")
                
                // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
                //, translate(lang, "history.dataChanged",{id:this.id})
            }
        }
    }
    
    sendMockdataTable = () => {
        this.sendDataToNext(DATA_TABLE,this.data,100)
        
    }

    changeStatus = (status:string, statusMessage:string) => {
        this.status=status
        this.statusMessage = statusMessage
    }

    clone = (id=this.flowNodeId) => {
        let clone = new InputCopy(id)
        
        clone.id = "Logic-"+id
//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage




        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }
}