import { PRE_TOKENIZE } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV, DATA_TOKENS, DATA_TEXT_DATASET } from "../../constants/dataTypes";
import { CLEAN, SELECT_CLEAN_METHOD, SERVER_REQUEST_TOKENIZE } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { getApi, postApi } from "../../services/server"
import * as url from "../../constants/url"
import { translate } from "../../languages/languages"
import { useStore } from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"


export class PreServerTokenize {
    id: string
    nodeType: string = PRE_TOKENIZE
    data: string[]
    dataType: string
    dataColumns: string[]
    selectedColumns: string[]
    flowNodeId: string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    status: string
    statusMessage: string
    serverData: {}
    errorHandlingType: string
    language: string
    targetColumn:string
    inplaceColumn: boolean
    newColumn:string
    columnsToDrop: string[]
    stop: boolean

    constructor(flowNodeId: string) {
        this.id = "Logic-" + flowNodeId
        this.data = []
        this.dataType = DATA_TEXT_DATASET
        this.dataColumns = []
        this.selectedColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [
            {
                source: "input",
                type: DATA_TEXT_DATASET,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ]
        this.outputs = [
            {
                name: "Raw data",
                type: DATA_TEXT_DATASET,
                next: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ],
            this.actions = [
                {
                    type: SERVER_REQUEST_TOKENIZE,
                    callback: () => this.serverTokenize(),
                    description: "Run cleaning process.",
                    autoRun: true,
                    icon: "SettingOutlined"
                },
                // {
                //     type:SELECT_CLEAN_METHOD,
                //     callback: () => this.selectCleanMethodCallback(),
                //     description: "Select type of cleaning process.",
                //     autoRun: false,
                //     icon: "SettingOutlined"
                // }
            ]
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.serverData = {}
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
        this.language = "english"
        
        this.targetColumn = "text"
        this.inplaceColumn = true
        this.newColumn = "text"
        this.columnsToDrop = []
        this.stop = false
    }

    clone = (id = this.flowNodeId) => {
        let clone = new PreServerTokenize(id)

//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage
        clone.language = this.language
        clone.errorHandlingType = this.errorHandlingType
        
        clone.targetColumn = this.targetColumn
        clone.inplaceColumn = this.inplaceColumn
        clone.newColumn = this.newColumn
        clone.columnsToDrop = this.columnsToDrop


        return clone
    }

    changeLanugage = (lang) => {
        //console.log(lang)
        this.language = lang //[english|czech]
    }

    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    changeTargetColumn = (targetColumn) => {
        //console.log(targetColumn)
        this.targetColumn = targetColumn //string
    }

    changeInplaceColumn = (inplaceColumn) => {
        //console.log(inplaceColumn)
        this.inplaceColumn = inplaceColumn //[true|false]
    }

    changeNewColumn = (newColumn) => {
        //console.log(newColumn)
        this.newColumn = newColumn //string
    }

    changeColumnsToDrop = (columnsToDrop) => {
        //console.log(columnsToDrop)
        this.columnsToDrop = columnsToDrop //string[]
    }

    tryToFix = () => {
        //console.log("under construction...")
    }
    
    canBeActionCalled = () => {
        //console.log(this.errorHandlingType)
        
        if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_IGNORE){
            //console.log("ERROR_HANDLING_IGNORE")
            return true
        }

        const typeOfData = typeof this.data.payload[0].text
        //console.log(typeOfData)
        
        let error = false
        let errorMessage = ""

        if (typeOfData !== "string"){
            error = true
            errorMessage = "status.datasetNotObject"
        }

        if (error){
            if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_TRY_TO_FIX){
                this.changeStatus(statusTypes.STATUS_ERROR, errorMessage)
                
                //console.log("ERROR_HANDLING_TRY_TO_FIX")
                this.tryToFix()
                return false
            }
            
            // if (this.errorHandlingType === errorHandlingTypes.ERROR_HANDLING_STOP){
                //console.log("ERROR_HANDLING_STOP")
                
                this.changeStatus(statusTypes.STATUS_ERROR, errorMessage)
                return false
            // }

        }
        //console.log("NON-ERROR-NON-IGNORE")

        return true

    }

    //dodělat ukládání do histore změna dat - // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
    serverTokenize = async () => {

        //canBeActionCalled je funkce, která kontroluje možné chyby, podle typu ošetření chybovosti může se pokusit chybu opravit, zastaví funkci, nebo chybu bude ignorovat
        // pokud chyba nenastane spustí funkci
        if (this.canBeActionCalled()){
            let dataId = ""
            for (let input of this.inputs){
                if (input.type === DATA_TEXT_DATASET){
                    dataId = input.data._id
                }
            }
            //vytvoří se tělo požadavku na server a dynamicky se dosadí hodnoty z aplikace
            const body = {
                "workspace_id": useStore().activeFlowId,
                "user_id": useStore().activeUser.token,
                "input_data_id": dataId,
                // "input_data_id": this.data._id,
                "task": "word_tokenizer",
                "lang": this.language,
                "target_column": this.targetColumn,
                "inplace_column": this.inplaceColumn,
                "new_column_name": this.newColumn,
                "columns_to_drop": this.columnsToDrop
                // "target_column": "text",
                // "inplace_column": "True",
                // "new_column_name": "text",
                // "columns_to_drop": [
                //     "text"
                // ]
            }
            //console.log(body)
            //tento požadavek odesílá výše uvedené tělo a na serveru spouští příslušný task
            let response = await postApi(url.URL_LIST_OF_TASKS, body)
            //pokud se task spustil
            if (response.status !== 422) {

                this.status = statusTypes.STATUS_RUNNING



                //uzel se označí jako běžící proces
                this.statusMessage = "status.requestServer"
                //vyvolá překreslení uzlu na plátně
                useStore().flowFunctions.refreshNodeWithSameLogicNode(this.flowNodeId, this)
                this.serverData = response.data
                //volá se funkce checkStatus, která se po jedné sekundě ptá serveru na stav spuštěného tasku vrací
                //status - zpráva ze serveru jak se task ukončil
                const { status, statusMessage } = await this.checkStatus()
                //nastaví se status do uzlu
                this.status = status
                // //console.log(status, statusMessage)

                this.statusMessage = statusMessage
            } else {
                //v případě že server odpoví statusem 422 - nezpracovatelná entita- nastaví se příslušný status
                this.statusMessage = "status.e422"
                this.status = statusTypes.STATUS_ERROR


            }
            // //console.log(this)
            //po změně uzlu se nechá čas, aby se vše všude propsalo a následně je vyvoláno překreselní plátna
            //s aktuálními daty
            let timeOut = setTimeout(() => {
                useStore().flowFunctions.refreshNodeWithSameLogicNode(this.flowNodeId, this)
            }, 1000)
        }
    }

    checkStatus = async () => {
        let response
        //v časovači má this jiný kontext a protože aktuální kontext je potřeba si zachovat i uvnitř
        //časovače je zde uložen do proměnné, která bude dostupná i uvnitř časovače
        let self = this
        //funkce vrací Promise, který je hotový až task skončí
        return await new Promise(function (resolve, reject) {
            //nastavení intervalu, ve kterém se bude klient ptát serveru
            let interval = setInterval(async () => {
                //dotaz o stavu daného tasku
                response = await getApi(url.URL_LIST_OF_TASKS, self.serverData._id).then(async(response) => {
                    //callback - jakmile server odpoví spouští se tento kus kódu
                    //console.log(response)
                    //pokud status tasku je "started" proces stále běží - za sekundu se časovač zeptá znovu
                    if (response.status !== "started"){
                        //pokud je task v jiném stavu pomocí switch-case se vybere příslušná akce
                        switch (response.status) {
                            case "Dataset not found":
                                //některé chybové hlášky jsou známé a jsou pro ně vytvořeny statusy,
                                // časovač se ukonnčí a funkce checkStatus vrací hodnotu statusu - selhání
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.datasetNotFound"
                                })
                                break;
                            case "Task failed: Target column is not present in dataset.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.columNoInData"
                                })
                                break;
                            case "Task failed: Target column values must be a string.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.targetColumnMustBeString"
                                })
                                break;
                            case "Task failed: Target column values must be a list.":
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.targetColumnMustBeList"
                                })
                                break;
                            case "succeeded":
                                //jediný úspěšný status, stejně jako u selhání, se ukončí časovač
                                //a vrací se status - úspěch, navíc
                                clearInterval(interval)

                                //console.log(response)
                                //při úspěšném dokončení tasku je položen dotaz na server, aby vrátil nově vzniklý dataset
                                let newD = await getApi(url.URL_SPECIFIC_DATASET(response.output_data_id))

                                //console.log(newD)
                                    resolve({
                                        status: statusTypes.STATUS_DONE,
                                        statusMessage: "status.succeeded"
                                    })
                                    //nově vzniklý dataset se propíše do dat uzlu i do outputů
                                    self.data = newD
                                    for (let output of self.outputs) {
                                        if (output.type === DATA_TEXT_DATASET) {
                                            output.data = newD
                                            output.progress = 100
                                        }
                                    }
                            
                                break;
                        
                            default:
                                //neznámá chyba funguje stejně ale vypisuje pouze obecnou chybu
                                clearInterval(interval)
                                resolve({
                                    status: statusTypes.STATUS_ERROR,
                                    statusMessage: "status.unknownError"
                                })
                                break;
                        }
                    }
                })
            
                if (self.stop){
                    
                    clearInterval(interval)
                    resolve({
                        status: statusTypes.STATUS_ERROR,
                        statusMessage: "status.responseTooLong"
                    })
                }
            }, 1000)
        });
    }

    // checkStatus = async () => {
    //     let response
    //     let self = this
    //     return await new Promise(function(resolve, reject) {
    //         let interval = setInterval(async ()=>{
    //             response = await getApi(url.URL_TASK,self.serverData._id).then((response)=>{
    //                 if (response.status === "Dataset not found"){
    //                     clearInterval(interval)
    //                     resolve(statusTypes.STATUS_ERROR)
    //                 }
    //             })

    //            

    //         }, 1000)
    //     });



    // }



    stopProcess = () => {
        this.stop = true

        setTimeout(()=>{
            this.stop = false
        },3000)
    }

    changeStatus = (status: string, statusMessage: string) => {
        this.status = status
        this.statusMessage = statusMessage
    }

    resetInput() {
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            }
        ]
    }
    onInput(inputType, data, dataColumns) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }


    getDataFromInput(inputType: string) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                this.data = i.data
                this.dataColumns = i.dataColumns
            }
        }
    }

} 