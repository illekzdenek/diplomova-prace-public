import { INPUT_SERVER } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV, DATA_TEXT_DATASET } from "../../constants/dataTypes";
import { CLEAN, SELECT_CLEAN_METHOD, REMOVE, SEND_RUN_TASK, SELECT_DATASET } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { translate } from "../../languages/languages"
import {useStore} from "../../stores/globalStore"
import { getApi } from "../../services/server";
import { URL_LIST_OF_DATASETS } from "../../constants/url";
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"



export class InputServer {
    id: string
    nodeType: string = INPUT_SERVER
    data: {}[]
    dataType: string
    dataColumns: string[]
    selectedColumns: string[]
    flowNodeId:string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    status: string
    statusMessage: string
    errorHandlingType: string
    datasetList: {}[]

    constructor(flowNodeId:string){
        this.id = "Logic-"+flowNodeId
        this.data = []
        this.dataType = DATA_TEXT_DATASET
        this.dataColumns = []
        this.selectedColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = []
        this.outputs = [
            {
                name:"Raw data",
                type: DATA_TEXT_DATASET,
                columns: this.selectedColumns,
                next: [],
                data: [],
                dataColumns:[],
                progress: 0
            }
        ],
        this.actions = [
        ]
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
        this.datasetList = []
    }
    
    clone = (id=this.flowNodeId) => {
        let clone = new InputServer(id)
        
//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage




        return clone
    }

    getDatasetsList = async () => {
        let datasetList = await getApi(URL_LIST_OF_DATASETS)
        this.datasetList = datasetList
        return datasetList
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    onInput (inputType, data, dataColumns){
        for (let i of this.inputs){
            if (i.type === inputType){
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }

    selectColumns (selectedColumns:string[]) {
        this.selectedColumns = selectedColumns
    }

    
    sendDataToNext = (outputType, data, progress) => {
    
        for (let output of this.outputs) {
            if (output.type === outputType){
                output.data = data,
                output.progress = progress
                output.dataColumns = this.dataColumns
                this.changeStatus(statusTypes.STATUS_DONE,"status.done")
                // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
            }
        }
    }
    
    sendData = (data) => {
        
        this.data = data
        this.sendDataToNext(DATA_TEXT_DATASET, data, 100)
        
    }
    
    changeStatus = (status:string, message:string) => {
        this.status=status
        this.statusMessage = message
    }

} 