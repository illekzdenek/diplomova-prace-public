import { PRE_CLEANING } from "../../constants/nodeTypes";
import { DATA_TABLE, DATA_CSV } from "../../constants/dataTypes";
import { CLEAN, SELECT_CLEAN_METHOD, REMOVE } from "../../constants/actionTypes";
import * as statusTypes from "../../constants/statusTypes"
import { translate } from "../../languages/languages"
import {useStore} from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"


export class PreCleaning {
    id: string
    nodeType: string = PRE_CLEANING
    data: {}[]
    dataType: string
    dataColumns: string[]
    selectedColumns: string[]
    flowNodeId:string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    status: string
    statusMessage: string
    cleaningType: string
    errorHandlingType: string

    constructor(flowNodeId:string){
        this.id = "Logic-"+flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.selectedColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns:[],
                progress: 0
            },
            // {
            //     source: "input",
            //     type: DATA_CSV,
            //     previous: [],
            //     data: "",
            //     progress: 0
            // },
        ]
        this.outputs = [
            {
                name:"Raw data",
                type: DATA_TABLE,
                columns: this.selectedColumns,
                next: [],
                data: [],
                dataColumns:[],
                progress: 0
            }
            // ,{
            //     name:"Raw data",
            //     type: DATA_TABLE,
            //     columns: this.selectedColumns,
            //     next: [],
            //     data: "",
            //     progress: 0
            // },
        ],
        this.actions = [
            {
                type:CLEAN,
                callback:() => this.cleanDataCallback(),
                description: "Run cleaning process.",
                autoRun: true,
                icon: "SettingOutlined"
            },
            {
                type:SELECT_CLEAN_METHOD,
                callback: () => this.selectCleanMethodCallback(),
                description: "Select type of cleaning process.",
                autoRun: false,
                icon: "SettingOutlined"
            }
        ]
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.cleaningType = REMOVE
        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }
    
    clone = (id=this.flowNodeId) => {
        let clone = new PreCleaning(id)
        
//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage




        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    changeStatus = (status:string, statusMessage:string) => {
        this.status=status
        this.statusMessage = statusMessage
    }

    resetInput () {
       
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns:[],
                progress: 0
            }
        ]
       
    }
    onInput (inputType, data, dataColumns){
        for (let i of this.inputs){
            if (i.type === inputType){
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }

    selectColumns (selectedColumns:string[]) {
        this.selectedColumns = selectedColumns
    }

    selectCleanMethodCallback(cleaningType:string){
        this.cleaningType = cleaningType
    }

    // cleanDataCallback (cleaningType:string=REMOVE){
    //     this.status = statusTypes.STATUS_RUNNING,
    //     this.statusMessage = "status.cleaning"
    //     let clone = this.clone()
    //     switch (clone.cleaningType) {
    //         case REMOVE:
    //             let cleansedData = []
    //             let complete = true
    

    //             for (let data of clone.data){
    
    //                 for (let item in data){
    //                     if (data[item] === [] || data[item] === "" || data[item] === null){
    //                         complete = false
    //                     }
    //                 }
    //                 if (complete){
    //                     cleansedData.push(data)
    //                 }
    //                 complete = true
    //             }
    //             clone.data = cleansedData
    
    //             for (let output of clone.outputs){
    //                 if (output.type === DATA_TABLE){//&& this.data.length>0
    //                     output.data = cleansedData
    //                     output.progress = 100
    //                     output.dataColumns = clone.dataColumns
    //                 }
    //                 if (clone.data.length === 0){
    //                     output.progress = 0
    //                 }
    //             }

    //             if (clone.data.length === 0){
    
    //                 clone.status = statusTypes.STATUS_ERROR,
    //                 clone.statusMessage = "status.noDataInput"
    //             } else {
    
    //                 clone.status = statusTypes.STATUS_DONE,
    //                 clone.statusMessage = "status.done"
                    
    //                 // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
    //             }
    //             return clone;
        
    //         default:
                
    //             break;
    //     }
    // }

    cleanDataCallback (cleaningType:string=REMOVE){
        this.status = statusTypes.STATUS_RUNNING,
        this.statusMessage = "status.cleaning"
        // let clone = this.clone()
        switch (this.cleaningType) {
            case REMOVE:
                let cleansedData = []
                let complete = true
                

                for (let data of this.data){
                    
                    for (let item in data){
                        if (data[item].length === 0 || data[item] === "" || data[item] === null){
                            complete = false
                        }
                    }
                    if (complete){
                        cleansedData.push(data)
                    }
                    complete = true
                }
                this.data = cleansedData
                
                for (let output of this.outputs){
                    if (output.type === DATA_TABLE){//&& this.data.length>0
                        output.data = cleansedData
                        output.progress = 100
                        output.dataColumns = this.dataColumns
                    }
                    if (this.data.length === 0){
                        output.progress = 0
                    }
                }

                if (this.data.length === 0){
                    this.status = statusTypes.STATUS_ERROR,
                    this.statusMessage = "status.noDataInput"
                } else {
                    this.status = statusTypes.STATUS_DONE,
                    this.statusMessage = "status.done"
                    

                    // useStore().flowFunctions.saveToHistory(useStore().flowInstance.toObject(), false, true, translate(useStore().settings.language, "history.dataChanged",{id:this.flowNodeId}))
                
                }
        
            default:
                
                break;
        }
    }

    getDataFromInput (inputType:string){
        for (let i of this.inputs){
            if (i.type === inputType){
                this.data = i.data
                this.dataColumns = i.dataColumns
                
            }
        }
    }

    getData (){
        setTimeout(()=>{
            this.data = [
                {
                    name: "B",
                    age: 27,
                    job: "Lawyer"
                },
                {
                    name: "C",
                    age: 51,
                    job: "Doctor"
                },
                {
                    name: "D",
                    age: 45,
                    job: "driver"
                },
            ]
            this.dataColumns = ["name", "age", "job"]
        },1000)
    }

} 