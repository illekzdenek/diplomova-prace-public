import { OUTPUT_CHART, } from "../../constants/nodeTypes";
import { DATA_TABLE } from "../../constants/dataTypes";
import * as statusTypes from "../../constants/statusTypes"
import { CONSOLE_LOG } from "../../constants/actionTypes";
import { translate } from "../../languages/languages"
import { useStore } from "../../stores/globalStore"
import * as errorHandlingTypes from "../../constants/errorHandlingTypes"

// import {useStore} from "../../stores/globalStore"
// const store = useStore()
export class OutputChart {
    id: string
    nodeType: string = OUTPUT_CHART
    data: {}[]
    dataType: string
    dataColumns: string[]
    flowNodeId: string
    outputs: {}[]
    inputs: {}[]
    actions: {}[]
    selections: {}
    status: string
    statusMessage: string
    errorHandlingType: string

    constructor(flowNodeId: string) {
        this.id = "Logic-" + flowNodeId
        this.data = []
        this.dataType = DATA_TABLE
        this.dataColumns = []
        this.flowNodeId = flowNodeId
        this.inputs = [
            {
                source: "input",
                type: DATA_TABLE,
                previous: [],
                data: [],
                dataColumns: [],
                progress: 0
            },
            // {
            //     source: "input",
            //     type: DATA_CSV,
            //     previous: [],
            //     data: "",
            //     progress: 0
            // }
        ]
        this.outputs = []
        this.status = statusTypes.STATUS_READY
        // this.statusMessage = "Ready to work."
        this.statusMessage = "status.ready"
        this.actions = []
        this.selections = {
            first: "",
            second: "",
            mode: "default"
        }

        this.errorHandlingType = errorHandlingTypes.ERROR_HANDLING_IGNORE
    }

    onInput(inputType, data, dataColumns) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                i.data = data
                i.dataColumns = dataColumns
            }
        }
    }

    changeStatus = (status: string, statusMessage: string) => {
        this.status = status
        this.statusMessage = statusMessage
    }


    getDataFromInput(inputType: string) {
        for (let i of this.inputs) {
            if (i.type === inputType) {
                this.data = i.data
                this.dataColumns = i.dataColumns
                if (i.data.length === 0) {
                    this.status = statusTypes.STATUS_ERROR,
                        this.statusMessage = "status.noDataInput"
                } else {
                    this.status = statusTypes.STATUS_DONE,
                        this.statusMessage = "status.done"
                }
            }
        }
    }

    clone = (id = this.flowNodeId) => {
        let clone = new OutputChart(id)

//každá nová vlastnost se musí přidat i do clone metody
        clone.data = this.data
        clone.dataColumns = this.dataColumns
        clone.inputs = this.inputs
        clone.outputs = this.outputs
        clone.status = this.status
        clone.statusMessage = this.statusMessage



        clone.selections = this.selections

        return clone
    }
    
    changeErrorHandlingType = (errorType) => {
        //console.log(errorType)
        this.errorHandlingType = errorType //[ERROR_HANDLING_IGNORE|ERROR_HANDLING_TRY_TO_FIX|ERROR_HANDLING_STOP]
    }

    setSelections(selections) {
        this.selections = selections
    }
} 