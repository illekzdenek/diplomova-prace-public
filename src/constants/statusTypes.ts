


export const STATUS_READY = "STATUS_READY"
export const STATUS_ERROR = "STATUS_ERROR"
export const STATUS_DONE = "STATUS_DONE"
export const STATUS_RUNNING = "STATUS_RUNNING"


export const STATUS_READY_COLOR = "lightblue"
export const STATUS_ERROR_COLOR = "red"
export const STATUS_DONE_COLOR = "green"
export const STATUS_RUNNING_COLOR = "orange"