export const DEV_MODE = false //false zakazuje některá tlačítka a informace, které slouží pro ladění a vývoj klienta, true - použít při vývoji, je dostupnější důležitá data pro vývoj a jednodušší možnosti ladění


//inicializační hodnoty nastavení
export const SETTINGS_AUTO_SAVE_ON_SWITCH = true
export const SETTINGS_AUTO_RUN = false
export const SETTINGS_DISPLAY_ACTION_BUTTONS = true
export const SETTINGS_DISPLAY_MODALS = false
export const SETTINGS_NUMBER_OF_TABS = 2
export const SETTINGS_HANDLE_SIZE = 4
export const SETTINGS_ACCEPT_MODAL_ALLOWED = true
export const SETTINGS_LANG = "en" // cs | en
export const SETTINGS_MESSAGE_ON = false
export const SETTINGS_MESSAGE_TIME = 1
export const SETTINGS_ON_BOARDING_ON = true
export const SETTINGS_LENGTH_OF_TEXT = 60
export const SETTINGS_NODE_STYLE = "alpha" //alpha | clasic


export const URL_ROOT = "https://mta.pef.mendelu.cz/lab-api/"       //list všech dostupných endpointů

