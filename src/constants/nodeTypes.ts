export const TYPE_FEATURE_NAME = "type_feature_name";
export const INPUT_PRESET = "input_preset";
export const INPUT_COPY = "input_copy";
export const INPUT_SERVER = "input_server";


export const INPUTS = [INPUT_PRESET, INPUT_COPY, INPUT_SERVER];

//pre-process
export const PRE_CLEANING = "pre_cleaning";
export const PRE_SPLIT = "pre_split";
export const PRE_SELECT = "pre_select";
export const PRE_TOKENIZE = "pre_tokenize";
export const PRE_STOP_WORDS = "pre_stop_words";
export const PRE_LEMMATIZE = "pre_lemmatize";
export const PRE_POS_TAGGER = "pre_pos_tagger";
export const PRE_DROP_COLUMNS = "pre_drop_columns";
export const PRE_PRE_PROCESS = "pre_pre_process";

export const PRE_PROCESSES = [PRE_CLEANING, PRE_SPLIT, PRE_SELECT, PRE_TOKENIZE, PRE_STOP_WORDS, PRE_PRE_PROCESS];

//process
export const PROCESS_TRAIN_LINEARREGRESION = "process_train_linearRegresion";

export const PROCESSES = [PROCESS_TRAIN_LINEARREGRESION];

//output
export const OUTPUT_LOG_CONSOLE = "output_log_console";
export const OUTPUT_CHART = "output_chart";

export const OUTPUTS = [OUTPUT_LOG_CONSOLE, OUTPUT_CHART];