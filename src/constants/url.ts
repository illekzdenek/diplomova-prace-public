

import { URL_ROOT } from "./config"
// export const URL_ROOT = "https://mta.pef.mendelu.cz/lab-api/"       //list všech dostupných endpointů

export const URL_MAIN_ROUTE = URL_ROOT
export const URL_LIST_OF_DATASETS = URL_ROOT+"dataset/"     //list všech dostupných datasetů
export const URL_LIST_OF_TASKS = URL_ROOT+"tasks/"       //list všech dostupných tasků
export const URL_API_DOC = URL_ROOT+"api-doc"       //dokumentace
export const URL_DOCS_OAUTH = URL_ROOT+"docs/oauth2-redirect"       //dokumentace
export const URL_REDOC = URL_ROOT+"redoc"       //dokumentace

export const URL_SPECIFIC_TASK = (id) => {       //vrací konkrétní task na základě ID
    return  URL_ROOT+"tasks/"+id
}
export const URL_SPECIFIC_DATASET = (id) => {       //vrací konkrétní dataset na základě ID
    return  URL_ROOT+"dataset/"+id
}


export const URL_USERS = URL_ROOT+"users/"
export const URL_USERS_AUTH = URL_USERS+"auth"


export const URL_COMPONENTS = URL_ROOT+"components/"
export const URL_FILES = URL_ROOT+"files/"

export const URL_OPEN_API = URL_ROOT+"openapi.json"
