import { Background } from "@braks/vue-flow"





// Global variables
export const lessVariablesGreen = {
    // mainColor: "#84DCCF",
    // mainColorLight: "#00aeff",
    // lightColor: "#F6E8EA",
    // darkColor: "#22181C",
    // darkColorSecondary: "#312F2F",
    // differentColor: "#EF626C",
    // white: "white",


    // secondaryColor: "#42b983",
    // secondaryColorLight: "#42b983",
    // secondaryColorDark: "#42b983",

    // gradient: "linear-gradient(45deg, #00aeff, #a68eff)",
    // textColor: "white"
}

export const lessVariablesYellowGreen = {
    // mainColor: "#c3d52a",
    // mainColorLight: "#e0f531",
    // lightColor: "#a6b524",
    // darkColor: "#141604",
    // darkColorSecondary: "#31350b",
    // differentColor: "#2a1e95",
    // white: "white",


    // secondaryColor: "#952a1e",
    // secondaryColorLight: "#c2675d",
    // secondaryColorDark: "#b3200f",

    // gradient: "linear-gradient(45deg, #00aeff, #a68eff)",
    // textColor: "white"
}
export const lessVariablesRed = {
    // mainColor: "#7B0D1E",
    // mainColorLight: "#9F2042",
    
    // lightColor: "#F8E5EE",

    // darkColor: "#211103",
    // darkColorSecondary: "#3D1308",

    // differentColor: "#2a1e95",
    // white: "white",


    // secondaryColor: "#9F2042",
    // secondaryColorLight: "#D5345F",
    // secondaryColorDark: "#66152A",

    // gradient: "linear-gradient(45deg, #00aeff, #a68eff)",
    // textColor: "white"
}

export const lessVariablesPastel = {
    // mainColor: "#B8D8BA",
    // mainColorLight:"#D8E9D9", 
    
    // lightColor:"#9BA253",

    // darkColor:"#69585F",
    // darkColorSecondary:"#594A50",

    // differentColor: "#EF959D",
    // white: "white",


    // secondaryColor: "#FCDDBC",
    // secondaryColorLight:"#FDEBD8",
    // secondaryColorDark: "#FACE9E",

    // gradient: "linear-gradient(45deg, #00aeff, #a68eff)",

    // textColor: "#69585F",
    // textColorLight: "white"
}

export const lessVariablesUniversity = {
    // mainColor: "#B8D8BA",
    // mainColorLight:"#D8E9D9", 
    
    // lightColor:"#9BA253",

    // darkColor:"#69585F",
    // darkColorSecondary:"#594A50",

    // differentColor: "#EF959D",
    // white: "white",


    // secondaryColor: "#FCDDBC",
    // secondaryColorLight:"#FDEBD8",
    // secondaryColorDark: "#FACE9E",

    // gradient: "linear-gradient(45deg, #012A4A, #A9D6E5)",

    // textColor: "#69585F",
    // textColorLight: "white",

    //------------------------------
    background: "#A9D6E5",
    footer: "#01497C",
    header: "#01497C",

    activeElements: "#61A5C2",
    activeElementsLight: "#89C2D9",
    activeElementsDark: "#468FAF",

    accent: "#014F86",

    connectionOk: "green",
    connectionWrong: "red",

    tableBackground:"#89C2D9",
    tableHeader: "white",

    actionMenuNode: "A9D6E5",

    textColor: "#012A4A",
    textColorLight: "A9D6E5",

    gradient: "linear-gradient(45deg, #012A4A, #A9D6E5)",

}


export const lessVariablesOriginal = {
    background: "#1A192B",
    footer: "#1A192B",
    header: "#1A192B",

    activeElements: "#61A5C2",
    activeElementsLight: "#89C2D9",
    activeElementsDark: "#468FAF",

    accent: "#014F86",

    connectionOk: "green",
    connectionWrong: "red",

    tableBackground:"#89C2D9",
    tableHeader: "white",

    actionMenuNode: "white",

    textColor: "#012A4A",
    textColorLight: "A9D6E5",

    gradient: "linear-gradient(45deg, #012A4A, #A9D6E5)",

}
// export const lessVariablesPastel = {
//     mainColor: "",
//     mainColorLight:"", 
    
//     lightColor:"",

//     darkColor:"",
//     darkColorSecondary:"",

//     differentColor: "",
//     white: "white",


//     secondaryColor: "",
//     secondaryColorLight:"",
//     secondaryColorDark: "",

//     gradient: "linear-gradient(45deg, #00aeff, #a68eff)",

//     textColor: ""
// }
