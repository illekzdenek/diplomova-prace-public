export const cs = {
    title: "Český titulek",
    general: {
        cancel: "Zrušit",
        submit: "Potvrdit",
        ok: "Ano",
        realyChangeLanguage: "Opravdu chcete změnit jazyk? Možná mu nebudete rozumět.",
        realyCloseTab: "Opravdu chcete zavřít !%tabname ? Tato akce nelze později vrátit zpět.",
        warning: "Varování!",
        openHistory: "Otevřít historii",
        next: "Dále",
        previous: "Zpět",
        finish: "Dokončit",
        history: "Historie",
        runTask: "Spustit úlohu",
        czech: "Čeština",
        english: "Angličtina",
        language: "Jazyk",
        saveDataset: "Uložit dataset",
        runExample: "Spustit příklad",
        datasets: "Datasety",
        download: "Stáhnout",

    },
    dataset:{
        datasetName: "Název datasetu",
        clickToUpload: "Nahrát soubor",


    },
    messages:{
        succesLogin: "Úspěšné přihlášení",
        succesRegister: "Úspěšná registrace",
        unknownProblem: "Neznámý problém",
        userExist: "Uživatel již existuje",
        succesUpload: "Soubor !%filename byl nahrán úspěšně.",
        failedUpload: "Soubor !%filename se nenahrál.",

    },
    edge: {
        label: "Nadpis",
        run: "Spustit"
    },
    node: {
        title: "Uzel",
        color: "Barva",
        preprocessing: "Zpracování",
        input: "Vstup",
        output: "Výstup",
        untypped: "Bez typu",
        selectDataset: "Vybrat dataset",
        refresh: "Obnovit",
        
        previewOfSelectedData:"Náhled na vybraný dataset",
        activeDataset: "Náhled na aktivní data",
    },
    layout: {
        addParent: "Přidat rodiče",
        showEdges: "Vypsat hrany (console)",
        fitView: "Zobrazit všechny uzly na obrazovku"
    },
    status: {
        ready: "Připraven.",
        done: "Hotové.",
        noDataInput: "Na vstupu nejsou žádná data.",
        cleaning: "Čištění...",
        waitingForFilter: "Čekání na výběr filtrů.",
        requestServer: "Odesílá požadavek na server.",
        e422: "422 - Nezpracovatelná entita.",
        datasetNotFound: "Dataset nenalezen.",
        missingData: "Chybějící vstupní data.",
        invalidJSON: "Nesprávný formát JSON kódu.",
        succeeded: "Úspěch.",
        columNoInData: "Task selhal: cílový sloupec není datasetu.",
        targetColumnMustBeString: "Task selhal: Cílový sloupce musí být řetězec.",
        targetColumnMustBeList: "Task selhal: Cílový sloupce musí být seznam.",
        mustBeList: "Task failed: Target column values must be a list.",
        responseTooLong: "Úloha trvá nezvykle dlouho."

    },
    constants: {
        nodes: {
            input_preset: "Předpřipravená data",
            input_copy: "JSON vstup",
            input_server: "Předpřipravené datasety",

            pre_cleaning: "Čištění",
            pre_select: "Filtrování dat",
            pre_tokenize: "Tokenizace textu",
            pre_stop_words: "Stop words",
            pre_lemmatize: "Lemmatizatizace",
            pre_pos_tagger: "Označení částí textu",
            pre_drop_columns: "Odstranit sloupce",
            pre_pre_process: "Předzpracování",

            output_log_console: "Výpis do Logu",
            output_chart: "Čárový graf",
        }

    },
    settings: {
        mainMenu: "Hlavní nabídka",
        sectionDescription: {
            user: "Nastavení související s účtem.",
            preferences: "Nastavení související s fungováním softwaru.",
            detail: "Informace, články, o autorech, odkazy.",
            contact: "Formmulář a kontaktní údaje."
        },
        preferences: {
            actionButtons: "Zobrazovat akční tlačítka přímo u uzlů na plátně",
            modalDisplay: "Zobrazovat Modalové okno místo vysouvacího",
            autoTabSave: "Automatické ukládání při změně aktivního tabu",
            numberOfTabs: "Maximální počet tabů",
            acceptModalAllowed: "Zobrazení varovného okna před provedením nebezpečné akce",
            handleSize: "Velikost spojovacích bodů uzlů",
            messageTime: "Čas po kterém se vyskakovací okno se stisknutou klávesou sám zavře",
            messageOn: "Zapnout vyskakovací okna se sstisknutými klávesami",
            messageTitle: "Zprávy o klávesových zkratkách",
            handleSizeTitle: "Velikost úchytů",
            showOnboarding: "Představení / tutorial",
            lengthOfText: "Délka ukázkového textu",
            design: "Vzhled",
            nodeStyle: "Nový vzhled uzlů",
            autoRun: "Automatické spouštění úloh",


        },
        buttons: {
            user: "Uživatel",
            settings: "Nastavení",
            detail: "Detail",
            contact: "Kontakt",
            test: "Test"
        }

    },
    sideMenu: {
        inputs: "Vstupy",
        title: "Úkony",
        prepare: "Předzpracování",
        outputs: "Výstupy",
        close: "Zavřít",
        nodes: {
            preset: {
                title: "Testovací data",
                description: [
                    "Uzel, který poskytuje předpřipravené datasety",
                    "Pro účely testování"
                ],

            },
            copy: {
                title: "JSON",
                description: [
                    "Zdrojem dat je vložený JSON.",
                    "Vyžaduje columns a data formát.",
                    "Jako příklad můžete použít kód níže začínající '{' a končící '}'. Zkopírujte do vstupního pole a stiskněte parsovat."
                ],

            },
            serverInput: {
                title: "Datasety",
                description: [
                    "Testovací uzel pro načítání datasetů ze serveru."
                ],

            },
            preCleaning: {
                title: "Čistit",
                description: [
                    "Zpracovává řádek dat, kde některá informace chybí",
                    "Momentálně pouze odstraňuje řádky"
                ],

            },
            preSelect: {
                title: "Výběr řádků",
                description: [
                    "Umožňuje filtrovat sloupce z datasetu."
                ],

            },
            preTokenize: {
                title: "Tokenizace",
                description: [
                    "Z textového vstupu vrací pole slov.",
                    "Možnost zvolit si češtinu nebo angličtinu."
                ],

            },
            preStopwords:{
                title: "Odstranění stop words",
                description:[
                    "Základní stopwords.",
                    "Možnost zvolit si češtinu nebo angličtinu."
                ]
            },
            preLemmatize:{
                title: "Lemmatizace",
                description:[
                    "Lematizace textu",
                    "Možnost zvolit si češtinu nebo angličtinu."
                ]
            },
            prePosTagger:{
                title: "POS Tagger",
                description:[
                    "Part-of-speech rozpoznávání.",
                    "Možnost zvolit si češtinu nebo angličtinu.",
                ]
            },
            preDropColumns: {
                title: "Odstranit sloupce",
                description: [
                    "Odstraňuje vybrané sloupce.",
                ],
            },
            prePreProcess: {
                title: "Předzpracování",
                description: [
                    "Nejvíce používané techniky předzpracování",
                    "Odstranění čísel, emotikon, diakritiky, apod."
                ],
            },
            outputLog: {
                title: "Do konzole",
                description: [
                    "Vypíše data do vývojářské konzole v prohlížeči",
                    "Pro testování"
                ],

            },
            outputChart: {
                title: "Do grafu",
                description: [
                    "Zobrazuje tabulková data v grafu"
                ],

            },
        }
    },
    actionBar: {
        save: "Uložit",
        openMainMenu: "Otevřít hlavní menu",
        openTasks: "Otevřít nabídku úloh",
        closeTasks: "Zavřít nabídku úloh",
        uploadDataset: "Nahrát vlastní dataset",
        uploadDatasetModalTitle: "Nahrát soubor",
        start: "Spustit pipeline",
        whichDatasetSave: "Vyberte, které datasety se mají stáhnout.",
        saveToLocal: "Uložit dataset do lokálního souboru.",
        saveDataset: "Stáhnout",
    },
    dataPreview: {
        noData: "Žádná data",
        input: "Vstup",
        output: "Výstup",
        log: "Historie",
        empty: "Prázdné"
    },
    customNode: {
        data: "Data",
        lineChart: "Čárový graf",
        settings: "Nastavení",
        debug: "Data pro vývojáře",
        actions: "Akce",
        barChart: "Graf",
        title: "Titulek",
        addData: "Přidat mock Data",
        enabled: "Povoleno",
        disabled: "Zamčeno",
        color: "Barva pozadí",
        errorHandling: "Zpracování chyby",
        
        targetColumn: "Cílový sloupec",
        inplaceColumn: "Nahradit sloupec",
        newColumn: "Nový sloupec",
        columnsToDrop: "Sloupce k odstranění",
        columns: "Dostupné sloupce",

        removeStopwords: "Odstranit stopslova",
        useCustomStopwords: "Použít vlastní stopslova",
        customStopwords: "Vlastní stopslova",
        lemmatize: "Lemmatizace",
        toLowercase: "Do lowercase",
        removeNumbers: "Odstranit čísla",
        removeWordsContainingNumbers: "Odstranit slova obsahující čísla",
        removeEmoticons: "Odstranit emotikony",
        removeAccents: "Odstranit diakritiku",
        trimWhitespaces: "Oříznout bílé znaky",
        tokenizeResult: "Tokenizovat výsledek",
        columnsToRemove: "Sloupce k odstranění",

    },
    login: {
        login: "Přihlásit",
        password: "Heslo",
        name: "Jméno",
        register: "Registrovat",
        email: "Email"
    },
    logMessages: {
        fitView: "Pohled nastaven tak aby byli vidět všechny uzly.",
        languagesSwitch: "Jazyk byl změněn na: ",
        selectAll: "Vybrány všechny uzly.",
        selectedRemoved: "Vybrané uzly byli odstraněny.",
        edgeAdded: "Nová hrana s id: !%id byla přidána.",
        nodeAdded: "Nový uzel s id: !%id byl přidán.",
        nodeDuplicated: "Vytvořen nový uzel s id: !%id na základě uzlu s id : !%id2 .",
        flowLoaded: "Diagram byl načten.",
        saved: "Uloženo!",
        pipelineStarted: "Graf spuštěn - přidávání uzlů a hran je nyní zakázáno.",
        pipelineEnd: "Graf dokončen - nyní lze opět přidávat uzly i hrany - Čas: !%time sekund.",
    
    },
    dataTypes: {
        data_table: "Tabulková data (JSON)",
        data_tokens: "Tokeny",
        data_text_dataset: "Dataset"
    },
    onBoarding: {
        warningModalMessage: "Pokud si budete chtít projít rurotiál později, aktivujte ho v sekci nastavení a znovu načtěte aplikaci.",
        warningModalTitle: "Přeskočit tutoriál?",
        titles: {
            welcome: "Vítejte",
            canvas: "Plátno",
            language: "Jazyk",
            ending: "Závěr",
            minimap: "Minimapa",
            previewInputs: "Vstupní data",
            previewInputsSelect: "Výběr vstupu",
            previewOutput: "Výstupní data",
            log: "Log/Historie",
            addTab: "Nový tab",
            tab: "Tab/Pracovní plocha",
            settings: "Nastavení",
            nodesMenu: "Nabídka uzlů",
            //-------------settings------------------
            settingsPrefferences: "Preference",
            actionButtons: "Zobrazení akčních tlačítek",
            modalDisplay: "Zobrazení modalových oken",
            autoTabSave: "Automatické ukládání při přepnutí tabu",
            acceptModalAllowed: "Ochrana proti ztrátě dat",
            showOnboarding: "Prohlídka",
            numberOfTabs: "Maximální počet tabů",
            handleSize: "Velikost úchytů u uzlů",
            messageOn: "Klávesové zkratky",
            messageTime: "Čas klávesové zkratky",

        },
        descriptions: {
            welcome: "Právě jste se dostali k softwaru pro tvorbu vlastních úloh v oblasti zpracování přirozeného jazyka pomocí vizuálního programování. Tyto nápovědy Vám ukáží základní funkce softwaru, aby Váš start byl co nejjednodušší.",
            canvas: "Plátno je stěžejní částí aplikace. Zde se totiž odehrává všechno dění. Plátno slouží k tomu aby se na něj přidávali jednotlivé uzly a propojením několika uzlů vytvoříte vlastní úloh.",
            language: "Aplikace obsahuje jazyk češtinu a angličtinu. Tímto přepínačem lze přepnout na jiný jazyk.",
            ending: "Doufáme že se Vám aplikace bude líbit. Pokud by jste si někdy chtěli tuto prohlídku spustit znovu v nastavení můžete zaškrtnout že ji chcete spustit a při dalším načtení aplikace se Vám prohlídka spustí. Po aplikaci můžete narazit na tlačítka v podobě kruhu s ikonkou otazníku. Toto tlačítko spustí podobnou prohlídku, pro danou sekci. Chcete spustit ukázkový příklad?",
            minimap: "V rozsáhlejších projektech je možné se ztratit. Proto je tu tato minmapa, která Vám ukazuje váš pohled a co je v okolí mimo Váš pohled.",
            previewInputs: "Pokud je vybraný nějaký uzel, zde se zobrazují data, která má na vstupu.",

            previewInputsSelect: "Pokud má uzel více jak jeden vstup, je možné mezi nimi přepínat. Přepnutí nemá na uzel žádný vliv, pouze se přepne zobrazení vstupních dat v poli vstupních dat.",
            previewOutput: "Podobně jako u vedlejšího okna se zde zobrazují data. Tady se ale místo vstupních dat zobrazují výstupní data. Pokud má uzel více výstupů, lze mezi nimi přepínat stejně jako u vstupních dat.",
            log: "Log slouží hlavně jako výpis proběhlých akcí. Může být ale i použit pro výstup textových dat uživateli (Log uzel)",
            addTab: "Tímto tlačítkem můžete vytvořit nový čistý tab. Pokud aplikace dosáhne maximálního počtu otevřených tabů, tlačítko zmizí.",
            tab: "Tab nebo taky záložka slouží k zobrazení a manipulaci s pracovními plochamy. Uživatel může mít rozpracovaných více úloh a může mezi nimi přepínat pomocí kliknutí na příslušný tab. Tab má v pravo nahoře tlačítko pro zavření, pokud jsou 2 a více tabů otevřených.",
            settings: "Nastavení lze otevřít tímto tlačítkem. Je možné si zde zvolit preference, zapnout znovu tuto prohlídku, nebo se dozvědět více o aplikaci.",
            nodesMenu: " Tímto tlačítkem lze otevřít nabídka všech dostupných uzlů. Na levé straně obrazovky se zobrazí nabídka uzlů roztříděná do kategorií. Kategorie lze otevírat a zavírat. Uzly lze přetažením na Plátno začít používat.",
            //-------------settings------------------
            settingsPrefferences: "Tato sekce nastavení umožňuje si aplikaci přizpůsobit svému pracovnímu stylu, a preferencím, funkce položek Vám bude nastíněna v této prohlídce, a pokud by jste si potřebovali si něco osvěžit, můžete si ji kdykoliv projít znovu.",
            actionButtons: "Zobrazení akčních tlačítek přímu na uzlu. Vždy jsou akční tlačítka dostupná v detailu uzlu. Pro rychlejší manipulaci je ale možné tyto akce zobrazit přímo v uzlu a pro vykonání akce není nutné aby se musel každý uzel otevírat.",
            modalDisplay: "Tento software se snaží držet krok s dobou, někomu ale vyskakovací okna mohla přirůst k srdci, proto tu existuje možnost si zobrazit detail uzlu v modalovém okně namísto výsuvného draweru.",
            autoTabSave: "Abychom ušetřili výpočetní výkon Vašeho počítače, tak vždy je reálně aktivní pouze jedna z pracovních ploch, ostatní se buď odstraní, nebo uloží při přepnutí. Tato položka právě toto určuje. Jestli se má starý tab uložit, nebo zahodit neuložené změny.",
            acceptModalAllowed: "Některé akce nelze vrátit zpět a těmito akcemi by si měl být uživatel jistý na 100% že je chce vykonat. Takovéto akce chrání varovné vyskakovací okno, kde uživatel potvrdí že opravdu chce tuto akci vykonat. To může některé uživatle i zdržovat a proto je tu možnost si tyto varovné okna zapnout, nebo vypnout.",
            showOnboarding: "Pokud si přejete si znovu projít prohlídkou, stačí zapnout tuto možnost a znovu načíst aplikaci.",
            numberOfTabs: "Určuje kolik je možné mít otevřených tabů v jednu chvíli.",
            handleSize: "Úchyty u uzlů mohou být zvětšeny pro speciální případy, jako je viditelnost, zdravotní stav aj.",
            messageOn: "Klávesové zkratky fungují stále, pokud ale potřebujete aby se zobrazili v aplikaci, tak tato položka to umožňuje. Pro soukromou práci je možné toto vypnout, pro prezentační účely zapnout.",
            messageTime: "Pokud se zobrazují stisknuté klávesy, je možné si i zvolit za jak dlouho samy zmizí v sekundách.",
            



        }
    },
    
    onBoardingTutorial:{
        titles:{
            taskMenu: "Nabídka uzlů",
            nodeCategory: "Kategorie uzlů",
            dragableNode: "Přidání uzlu",
            node: "Uzel ",
            status: "Status uzlu",
            handle: "Úchyt",
            settings: "Nastavení",
            close: "Odstranění",
            title: "Název",
            category: "Kategorie",
            output: "Výstopní bod",
            input: "Vstupní bod",
            edge: "Hrana - tok dat",
            edgeRemove: "Odstranění hrany",
            taskSettings: "Nastavení úlohy",
            saveDataset: "Uložení datasetu",
            datasetNode: "Předpřipravené datasety",
            selectDataset: "Vybrat dataset",
            refresh: "Obnovit",
            selectedData: "Vybraná data",
            tokenize: "Tokenizace",
            availableColumns: "Dostupné sloupec",
            taskLanguage: "Jazyk úlohy",
            targetColumn: "Cílový sloupec",
            inplaceColumn: "Nahrazení sloupce",
            newColumn: "Nový sloupec",
            dropColumns: "Zahodit sloupce",
            runPipeline: "Spustit graf",
            upload: "Nahrát dataset" ,
            download: "Uložit do zařízení",
            ending: "Konec",

        },
        descriptions:{
            taskMenu: "Tento panel obsahuje nabídku všech dostupných úloh, roztříděnou podle typu úlohy, kterou představuje. Je možné si tento panel nechat otevřený. Skrýt nebo naopak zobrazit lze pomocí tlačítka 'Otevřít nabídku úloh' na nástrojové liště. ",
            nodeCategory: "Kategorie úlohy umožňují zlepšit přehlednost. Základní kategorie jsou Vstupy, Předzpracování a Výstupy. Po rozkliknutí kategorie uvidíte nabídku uzlů pro danou kategorii.",
            dragableNode: "Tato ohraničená oblast představuje uzel. Uzel se může přidat na plátno přetažením tohoto objektu na plátno.",
            node: "Přetažením uzlu nad plátno, se přidal příslušný uzel na plátno. Uzel je zde základní stavební kámen. Každý uzel reprezentuje jednu úlohu, nebo souhrn specifických NLP úloh. Další uzel lze vytvořit tak, že otevřete nabídku úloh a libovolnou úlohu přetáhnete na plátno. ",
            status: "Status ikonka označuje v jakém stavu je úloha. Modrá znamená že je nová a zatím se s ní nepracovalo, zelená, že vše proběhlo v pořádku, červená znamená chybu a žlutá znamená že úloha se stále zpracovává. Pokud chcete vědět o stavu více, umístěte kurzor nad tuto ikonku. Zobrazí se detailní popis stavu.",
            handle: "Úchyt slouží pro přemístění uzlu. Zde můžete kliknout a táhnout na libovolné místo na plátně. Po puštění tlačítka myši zde uzel zůstane.",
            settings: "Tímto tlačítkem je možné zobrazit detail uzlu. Jsou zde různé možnosti nastavení i informací o daném uzlu.",
            close: "Uzel se dá jednoduše odstranit kliknutím na toto tlačítko.",
            title: "Název úlohy kterou uzel představuje.",
            category: "Již zmíněné kategorie, pro zlepšení přehlednosti.",
            output: "Výstupní bod uzlu. Odtud můžete kliknutím a tažením nad libovolný vstupní bod uzlu vytvořit hranu a propojit tak dané dva uzly. Pokud je při tažení vstupní bod zelený, je možné hranu vytvořit, pokud je červený, něco vytvoření hrany brání. Výstupní bod může mít libovolný počet propojení.",
            input: "Vstupní bod je místem kam se směřuje tažení po kliknutí na výstupní bod. Podobně jako u výstupního bodu. Vstupní bod může mít pouze jedno propojení.",
            edge: "Pokud propojíte výše zmíněné body vznikne takováto hrana. Ta nemá žádnou specifickou funkci. Určuje ale, z jakého uzlu potečou data do jakého uzlu. Mezi každýma dvěma uzly může být pouze jedna hrana.",
            edgeRemove: "Hranu můžete libovolně odstranit kliknutím na toto tlačítko.",
            taskSettings: "Některé tasky můžou obsahovat konfiguraci. Pokud chcete použít defaultní konfiguraci, není potřeba nic měnit. Některé uzly mají více položek pro konfiguraci a některé méně. Vždy se ale položky zobrazují na stejném místě.",
            saveDataset: "Uložení datasetu na serveru. Aktuální dataset příslušného uzlu se uloží a bude dostupný mezi ostatními datasety (bude možné si jej stáhnout do PC přes modal stažení, nebo bude dostupný v Dataset uzlu a bude se moct použít v dalších grafech).",
            datasetNode: "Uzly obecně už umíte ovládat. Nyní se Vám představí některé konkrétní uzly. Jako první je dataset uzel. Nabízí uložené datasety, které si uživatel může nahrát, nebo uložit z libovolnéhu uzlu. Nemá vstupní bod protože sám je vstupním uzlem.",
            selectDataset: "Zde si můžete vybrat z dostupných datasetů. Při kliknutí se vám zobrazí seznam datasetů. Jejich název a datum uložení je hned viditelné, pro okamžitý náhled na data stačí nad danou položku seznamu umístit kurzor myši.",
            refresh: "Pokud jste právě nahráli dataset a ten se neobjevuje v seznamu, můžete požádat o obnovení seznamu. Pokud se podíváte znova pravděpodobně už dataset na seznamu bude.",
            selectedData: "Pokud jste si zvolili nějaký dataset ze seznamu, zde se Vám zobrazí náhled a název datasetu, máte tak na očích co za dataset posílá dále.",
            tokenize: "Uzel, který představuje úlohu tokenizace. Pokud o tokenizaci nic nevíte zkuste se podívat sem https://www.machinelearningplus.com/nlp/what-is-tokenization-in-natural-language-processing/",
            availableColumns: "Datasety jsou seznamem sloupců. Tyto sloupce představují řádky, nebo jiné oddělení dat. Tyto sloupce jsou pojmenované a můžete v datasetu mít vedle sebe několik sloupců. Zde je seznam dostupných sloupců v datech uzlu. Pokud žádné sloupce nemá, tak je to tím, že do uzlu netečou žádná data.",
            taskLanguage: "Jazyk prostředí a jazyk úlohy jsou dvě rozdílné věci. Jazyk úlohy určuje pouze to, jaký algoritmus se pro zpracování dané úlohy použije. Některé úlohy jsou totiž jiné pro různé jazyky.",
            targetColumn: "Do kterého sloupce se má výsledek úlohy zapsat. To buď založí nový sloupec, nebo přejmenuje používaný sloupec.",
            inplaceColumn: "Zda se daný sloupec má nahradit. Pokud je aktivované, výsledek bude pouze jeden sloupec, pokud bude deaktivovaný, musí být zadán název nového sloupce a výsledkem úlohy budou dva sloupce (s původními daty a s novými daty).",
            newColumn: "V případě, že se sloupce nemají nahrazovat, je nutné zadat název nového sloupce.",
            dropColumns: "Některé úlohy umožňují odstranit nepotřebné sloupce.",
            runPipeline: "Aplikace podporuje dávkový a automatický režim. Defaultně je aktivovaný dávkový režim. Uživatel si při něm může libovolně přidávat a spojovat uzly bez toku dat. Můžete si tak dopředu vše připravit a až si budete jistý tak tímto tlačítkem můžete celý graf spustit. Graf se spouští od vstupních uzlů a jakmile se jejich následníci zpracují, data se posílají dále až dojde graf nakonec. Pokud je graf spuštěn, není dovolené přidávat nové uzly, nebo propojovat uzly, nedoporučujeme ani měnit nastavení uzlů. Při spuštění se v tomto tlačítku zobrazí indikátor běhu grafu, dokud nezmizí graf stále běží. Do logu se zapisují zprávy o průběhu běhu. Spuštění se ukončí, když žádný graf není v běžícím stavu (žlutá barva). Je tedy možné že uzly skončí v pořádku (zelená), nebo skončí chybou (červeně), nepřipojené uzly mohou zůstat i modré. Pokud by běh grafu trval nepřiměřeně dlouho objeví se modalové okno, ve kterém můžete zvolit jestli chcete proces ukončit nebo počkat déle.",
            upload: "Toto tlačítko otevře modalové okno, které nabízí nahrání vlastního datasetu. Pro nahrání je nutné nejprve dataset pojmenovat. Následně nahrajete libovolný textový soubor. Jakmile je nahraný na server, objeví se potvrzovací zpráva. Dataset by měl být nahraný a objevit se mezi ostatními datasety. Možná bude potřebovat použít tlačítko obnovit v dataset uzlu." ,
            download: "Tlačítko otevírá modalové okno, které Vám umožní si vybrat datasety, které si chcete stáhnout do svého zařízení. Aby bylo možné dataset stáhnout, musí být uložený (nahraný, nebo uložený výsledek některé z úloh).",
            ending: "Doufáme, že vše bylo jasné. Aplikace je stále ve vývoji, a je tak možné, že narazíte na nějaké chyby. V takovém případě budeme rádi, když nás na chybu upozorníte. Děkujeme.",
            
        },
    },
    runningMode:{
        tooLongProcessing:"Proces trvá nečekaně dlouho, chcete uoknčit proces nebo dále čekat?",
        title:"Nečekaně dlouhý proces"
    }
}   