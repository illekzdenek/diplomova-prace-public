import {en} from "./en"
import {cs} from "./cs"

// export const translate = ( language: string, key:string) => {
//     const keyArray = key.split(".");
//     let lang = en
//     switch (language) {
//         case "cs":
//                 lang = cs
//             break;
//         case "en":
//                 lang = en
//             break;
    
//         default:
//             break;
//     }
//     for (let o in lang){
//         if (keyArray[0] && keyArray[0] === o){
//             if (keyArray[1] && typeof lang[o] === "object"){
//                 for (let i in lang[o]){
//                     if (keyArray[1] === i){
//                         return lang[o][i]
//                     }
//                 }
//             }
//             return lang[o]
//         }
//     }
//     return key
// }

export const translate = (language:string,key:string, data:{}|null=null)=>{
    const keyArray = key.split(".");
    let lang = en
    switch (language) {
        case "cs":
                lang = cs
            break;
        case "en":
                lang = en
            break;
    
        default:
            break;
    }
    for (let o in lang){
        if (keyArray[0] === o){
            let sentence = drillDown(lang[o],1, keyArray)
            //do sentence se vrací vyhledaná hodnota, může to být textový překlad, pole překladů, nebo poslední nalezená kategorie
            //poslední nalezená kategorie znamená nenalezený překlad a místo něj se vypíše orignální klíč
            if (JSON.stringify(sentence)[0] === "{"){
                return key
            }
            if (data !== null){
                for(let d in data){
                    sentence = sentence.replace("!%"+d, data[d]);
                }
            }
            return sentence
        }
    }
}

const drillDown = (root,level, keyArray) => {
    if (level < keyArray.length){
        for (let r in root){
            if (keyArray[level] === r){
                return drillDown(root[r],level+1, keyArray)
            }
        }
    }
    return root
}