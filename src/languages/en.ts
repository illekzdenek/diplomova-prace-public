export const en = {
  Title: "English title",
  general: {
    cancel: "Cancel",
    submit: "Submit",
    ok: "Ok",
    realyChangeLanguage:
      "Realy want to change the language? You could not understand.",
    realyCloseTab:
      "Realy do yo want to close !%tabname ? This action can not be turn back later.",
    warning: "Warning!",
    openHistory: "Open history",
    next: "Next",
    previous: "Previous",
    finish: "Finish",
    history: "History",
    runTask: "Run task",
    czech: "Czech",
    english: "English",
    language: "Language",
    saveDataset: "Save dataset",
    runExample: "Run example pipeline",
    datasets: "Datasets",
    download: "Download",


  },
  dataset:{
    datasetName: "Dataset name",
    clickToUpload: "Upload file",

  },
  messages:{
    succesLogin: "Succesful login",
    succesRegister: "Succesfuly registered",
    success: "Success",
    unknownProblem: "Unknown problem",
    userExist: "User already exist",
    succesUpload: "!%filename file uploaded succesfully.",
    failedUpload: "!%filename file uploaded failed.",

  },
  history: {
    edgeRemoved: "Removed edge: !%id ",
    nodeRemoved: "Removed node: !%id ",
    connected: "New edge added: !%id ",
    newNode: "New node added: !%id ",
    loaded: "Flow loaded.",
    edgeUpdated: "Edge updated !%id ",
    dataChanged: "Data in node !%id was changed ",
  },
  edge: {
    label: "Label",
    run: "Run",
  },
  node: {
    title: "Node",
    color: "Color",
    preprocessing: "Preprocessing",
    input: "Input",
    output: "Output",
    untypped: "Untypped",

    selectDataset: "Set dataset as active",
    refresh: "Refresh",
    previewOfSelectedData:"Data preview for selected dataset",
    activeDataset: "Active dataset preview",

  },
  layout: {
    addParent: "Add parent",
    showEdges: "Show edges (console)",
    fitView: "Fit view to all nodes",
  },
  status: {
    ready: "Ready to work.",
    done: "Done.",
    noDataInput: "No data on input.",
    cleaning: "Cleaning...",
    waitingForFilter: "Waiting for filters.",
    requestServer: "Requesting server",
    e422: "422 - Unprocessable Entity",
    datasetNotFound: "Dataset not found",
    missingData: "Missing input data.",
    invalidJSON: "Invalid JSON format.",
    succeeded: "Succeded.",
    columNoInData: "Task failed: Target column is not present in dataset.",
    targetColumnMustBeString:
      "Task failed: Target column values must be a string.",
    targetColumnMustBeList: "Task failed: Target column values must be a list.",
    mustBeList: "Task failed: Target column values must be a list.",
    responseTooLong: "Task processing is too long."
    
  },
  constants: {
    nodes: {
      input_preset: "Preset input node",
      input_copy: "JSON input node",
      input_server: "Prepared datasets",

      pre_cleaning: "Cleaning node",
      pre_select: "Filter data columns",
      pre_tokenize: "Text tokenization",
      pre_stop_words: "Stop words",
      pre_lemmatize: "Lemmatization",
      pre_pos_tagger: "Part of speech tagger",
      pre_drop_columns: "Drop columns",
      pre_pre_process: "Pre-Processing",

      output_log_console: "Output to Log",
      output_chart: "Line chart node",
    },
  },
  settings: {
    mainMenu: "Main menu",
    sectionDescription: {
      user: "User & account settings.",
      preferences: "User preferences.",
      detail: "Information about software, development, company & news.",
      contact: "Contact informations.",
    },
    preferences: {
      actionButtons: "Display action buttons on node",
      modalDisplay: "Display modal for task settings insteadof drawer",
      autoTabSave: "Auto save when active tab is changed",
      numberOfTabs: "Number of flow tabs",
      acceptModalAllowed:
        "Display warning modal windows before dangerous actions",
      handleSize: "Size of node handles",
      messageTime:
        "After this amount of seconds window with pressed keys will disapear",
      messageOn: "Turn on windows with pressed keys",
      messageTitle: "Keyboard info messages",
      handleSizeTitle: "Handle size",
      showOnboarding: "On boarding / tutorial",
      lengthOfText: "Length of preview text",
      design: "Design",
      nodeStyle: "New node style",
      autoRun: "Task autorun enabled",
    },
    buttons: {
      user: "User",
      settings: "Settings",
      detail: "Detail",
      contact: "Contact",
      test: "Test",
    },
  },
  sideMenu: {
    inputs: "Inputs",
    title: "Tasks",
    prepare: "Preprocessing",
    outputs: "Outputs",
    close: "Close",
    nodes: {
      preset: {
        title: "Test dataset",
        description: ["Node with existing dataset.", "For testing."],
      },
      copy: {
        title: "JSON",
        description: [
          "Node with JSON input.",
          "Requires column & data properties.",
          "You can copy code below as an example input starts with '{' and close with '}'",
        ],
      },
      serverInput: {
        title: "Datasets",
        description: ["Testing node for server fetching."],
      },
      preCleaning: {
        title: "Clean",
        description: [
          "When some data missing in some row, decide what to with these rows.",
          "Remove row.",
        ],
      },
      preSelect: {
        title: "Selecting columns",
        description: ["Filtering data columns."],
      },
      preTokenize: {
        title: "Tokenize",
        description: [
          "Tokenize string input into array output.",
          "Posibility choose czech or english language.",
        ],
      },
      preStopwords: {
        title: "Remove stop words",
        description: [
          "Basic stopwords.",
          "Posibility choose czech or english language.",
        ],
      },
      preLemmatize:{
          title: "Lemmatize",
          description:[
              "Lematize text",
              "Posibility choose czech or english language.",
          ]
      },
      prePosTagger:{
          title: "POS Tagger",
          description:[
              "Part-of-speech recognition.",
              "Posibility choose czech or english language.",
          ]
      },
      preDropColumns: {
        title: "Drop columns",
        description: [
          "Drop selected columns.",
        ],
      },
      prePreProcess: {
        title: "Pre-Processing",
        description: [
          "Most common preprocessing techniques.",
          "Remove numbers, emoticons, accents, etc."
        ],
      },
      outputLog: {
        title: "To console",
        description: ["Write data to console log", "For testing."],
      },
      outputChart: {
        title: "To chart",
        description: ["Display data to chart."],
      },
    },
  },
  actionBar: {
    save: "Save",
    openMainMenu: "Open main menu",
    openTasks: "Open tasks menu",
    closeTasks: "Close tasks menu",
    uploadDataset: "Upload dataset",
    uploadDatasetModalTitle: "Upload dataset",
    start: "Run pipeline",
    whichDatasetSave: "Select dataset to download.",
    saveToLocal: "Save dataset to local file.",
    saveDataset: "Download",

  },
  dataPreview: {
    noData: "No data",
    input: "Input",
    output: "Output",
    log: "Log",
    empty: "Empty",
  },
  customNode: {
    data: "Data",
    lineChart: "Line chart",
    settings: "Settings",
    debug: "Debug data for developers",
    actions: "Actions",
    barChart: "Bar chart",
    title: "Title",
    addData: "Add mock data",
    enabled: "Enabled",
    disabled: "Disabled",
    color: "Color",
    errorHandling: "Error handling",
    
    targetColumn: "Target column",
    inplaceColumn: "Inplace column",
    newColumn: "New column",
    columnsToDrop: "Columns to drop",
    columns: "Available columns",
    
    removeStopwords: "Remove stopwords",
    useCustomStopwords: "Use custom stopwords",
    customStopwords: "Custom stopwords",
    lemmatize: "Lemmatize",
    toLowercase: "To lowercase",
    removeNumbers: "Remove numbers",
    removeWordsContainingNumbers: "Remove words containing numbers",
    removeEmoticons: "Remove emoticons",
    removeAccents: "Remove accents",
    trimWhitespaces: "Trim whitespaces",
    tokenizeResult: "Tokenize result",
    columnsToRemove: "Columns to remove",


  },
  login: {
    login: "Login",
    password: "Password",
    name: "Name",
    register: "Register",
    email: "Email",
  },
  logMessages: {
    fitView: "View fitted to all nodes.",
    languagesSwitch: "Language set to: ",
    selectAll: "Selected all nodes.",
    selectedRemoved: "Selected nodes was removed.",
    edgeAdded: "New edge with id: !%id was added.",
    nodeAdded: "New node with id: !%id was added.",
    nodeDuplicated:
      "Created new node with id: !%id based on node with id : !%id2 .",
    flowLoaded: "Flow was loaded.",
    saved: "Saved!",
    pipelineStarted: "Pipeline start - add nodes and edges is restricted.",
    pipelineEnd: "Pipeline end - now you can add nodes and edges - Time: !%time seconds.",

  },
  dataTypes: {
    data_table: "Table data (JSON)",
    data_tokens: "Tokens",
    data_text_dataset: "Dataset",
  },
  onBoarding: {
    warningModalMessage:
      "If you want to view the rurotial later, activate it in the settings section and reload the app.",
    warningModalTitle: "Skip tutorial?",
    titles: {
      welcome: "Welcome.",
      canvas: "Canvas",
      language: "Language",
      ending: "Conclusion",
      minimap: "Minimap",
      previewInputs: "Input data",
      previewInputsSelect: "Input selection",
      previewOutput: "Output data",
      log: "Log/History",
      addTab: "New Tab",
      tab: "Tab/Work area",
      settings: "Settings",
      nodesMenu: "Node menu",
      //-------------settings------------------
      settingsPrefferences: "Preferences",
      actionButtons: "Display action buttons",
      modalDisplay: "View modal windows",
      autoTabSave: "Automatic saving when switching tabs",
      acceptModalAllowed: "Protection against data loss",
      showOnboarding: "Tour",
      numberOfTabs: "Maximum number of tabs",
      handleSize: "Size of handles at knots",
      messageOn: "Keyboard shortcuts",
      messageTime: "Shortcut key time",
    },
    descriptions: {
      welcome:
        "You've just come to software for creating your own natural language processing tasks using visual programming. These hints will show you the basic functions of the software to make your start as easy as possible",
      canvas:
        "The canvas is the core part of the application. This is where all the action happens. The canvas is used to add individual nodes to it, and by linking multiple nodes you create your own tasks.",
      language:
        "The app includes English and Czech. This switch can be used to switch to another language.",
      ending: "We hope you like the app. If you ever want to run this tour again, you can check the box in the settings to run it and the tour will run the next time you load the app. After the app you may come across buttons in the form of a circle with a question mark icon. This button will launch a similar tour, for that section. Do yo want to open example?",
      minimap:
        "It is possible to get lost in larger projects. That's why this mini-map is here, to show you your view and what's out of your view.",
      previewInputs:
        "If a node is selected, the data it has as input is displayed here",

      previewInputsSelect:
        "If a node has more than one input, it is possible to switch between them. Switching has no effect on the node, it only switches the display of the input data in the input data field.",
      previewOutput:
        "Similar to the adjacent window, data is displayed here. But here, instead of the input data, the output data is displayed. If a node has multiple outputs, you can switch between them just like with input data.",
      log: "The log serves mainly as a listing of the events that have taken place. However, it can also be used to output text data to the user (Log node)",
      addTab:
        "You can use this button to create a new clean tab. When the application reaches the maximum number of open tabs, the button disappears.",
      tab: "Tab is used to display and manipulate workspaces. The user can have multiple tasks in progress and can switch between them by clicking on the respective tab. The tab has a close button on the top right when 2 or more tabs are open.",
      settings:
        "The settings can be opened with this button. You can choose preferences, turn this tour back on, or learn more about the app.",
      nodesMenu:
        "This button can be used to open a menu of all available nodes. On the left side of the screen, a menu of nodes sorted into categories is displayed. The categories can be opened and closed. Nodes can be dragged to the Canvas to start using them",
      //-------------settings------------------
      settingsPrefferences:
        "This settings section allows you to customize the app to your working style, and preferences, the function of the items will be outlined to you in this tour, and you can always go through it again if you need a refresher.",
      actionButtons:
        "Display action buttons directly on the node. Action buttons are always available in the node detail. However, for faster manipulation, these actions can be displayed directly on the node and it is not necessary to open each node to perform the action.",
      modalDisplay:
        "This software tries to keep up with the times, but some people might find the popup windows too much, so there is an option to view the node detail in a modal window instead of a popup drawer",
      autoTabSave:
        "To save your computer's computing power, only one of the workspaces is actually active at any given time, the others are either removed or saved when you switch. This item specifies just that. Whether to save the old tab or discard unsaved changes.",
      acceptModalAllowed:
        "Some actions cannot be undone and the user should be 100% sure that he wants to perform them. Such actions are protected by a warning pop-up window where the user confirms that he really wants to perform the action. This can be a nuisance for some users, so there is an option to turn these warning windows on or off.",
      showOnboarding:
        "If you wish to revisit the tour, just turn on this option and reload the application",
      numberOfTabs: "Determines how many tabs can be open at one time.",
      handleSize:
        "Handles at knots may be enlarged for special cases such as visibility, medical conditions, etc.",
      messageOn:
        "Keyboard shortcuts still work, but if you need them to appear in the app, this item allows it. For private work this can be turned off, for presentation purposes it can be turned on",
      messageTime:
        "If the pressed keys are displayed, it is also possible to choose how long it takes for them to disappear in seconds",
    },
  },
    
  onBoardingTutorial:{
      titles:{
          taskMenu: "Node menu",
          nodeCategory: "NodeCategory",
          dragableNode: "Add node",
          node: "Node ",
          status: "Node status",
          handle: "Handle",
          settings: "Settings",
          close: "Delete",
          title: "Title",
          category: "Category",
          output: "Exit point",
          input: "Entry point",
          edge: "Edge - data flow",
          edgeRemove: "Edge removal",
          taskSettings: "Task Settings",
          saveDataset: "Saving dataset",
          datasetNode: "Pre-prepared datasets",
          selectDataset: "Select dataset",
          refresh: "Refresh",
          selectedData: "Selected data",
          tokenize: "Tokenize",
          availableColumns: "AvailableColumn",
          taskLanguage: "TaskLanguage",
          targetColumn: "TargetColumn",
          inplaceColumn: "Replace column",
          newColumn: "New column",
          dropColumns: "Drop Columns",
          runPipeline: "Run chart",
          upload: "Upload dataset" ,
          download: "Save to device",
          ending: "End",

      },
      descriptions:{
          taskMenu: "This panel contains a menu of all available tasks, sorted by the type of task it represents. It is possible to keep this panel open. You can hide or show it using the 'Open task menu' button on the toolbar. ",
          nodeCategory: "Task categories allow for improved clarity. The basic categories are Inputs, Preprocessing and Outputs. When you click on a category, you will see a menu of nodes for that category.",
          dragableNode: "This bounded area represents a node. A node can be added to the canvas by dragging this object over the canvas.",
          node: "By dragging a node over the canvas, the corresponding node is added to the canvas. The node is the basic building block here. Each node represents a single task, or a collection of specific NLP tasks. Another node can be created by opening the task menu and dragging any task onto the canvas. ",
          status: "The status icon indicates what state the task is in. Blue indicates that it is new and has not been worked on yet, green indicates that everything went fine, red indicates an error, and yellow indicates that the job is still being processed. If you want to know more about the status, place your cursor over this icon. A detailed description of the status will be displayed.",
          handle: "The handle is used to reposition the node. Here you can click and drag to any location on the canvas. When you release the mouse button, the node will remain there.",
          settings: "This button can be used to view the detail of the node. There are various options for settings and information about the node.",
          close: "The node can be easily removed by clicking on this button.",
          title: "The name of the task the node represents.",
          category: "The aforementioned categories, to improve clarity.",
          output: "The node's exit point. From here, you can click and drag over any node entry point to create an edge and connect the two nodes. If the entry point is green when dragged, the edge can be created, if it is red, something is preventing the edge from being created. An output point can have any number of connections.",
          input: "The entry point is where the drag is directed when the exit point is clicked. Similar to the exit point. An entry point can have only one link.",
          edge: "If you connect the above points, an edge like this is created. It has no specific function. But it determines from which node data flows to which node. There can only be one edge between any two nodes.",
          edgeRemove: "You can remove an edge at will by clicking this button.",
          taskSettings: "Some tasks may contain configuration. If you want to use the default configuration, there is no need to change anything. Some nodes have more items for configuration and some have less. But always the items are displayed in the same place.",
    
          saveDataset: "Storing the dataset on the server. The current dataset of the respective node will be saved and will be available among other datasets (it can be downloaded to PC via modal download, or it will be available in the Dataset node and can be used in other graphs).",
          datasetNode: "Nodes in general you can already control. Now you will be introduced to some specific nodes. The first is the dataset node. It offers saved datasets that the user can load or save from any node. It has no entry point because it is itself an entry node",
          selectDataset: "Here you can select from the available datasets. When clicked, you will see a list of datasets. Their name and save date is immediately visible, just hover over the list item to get an instant preview of the data.",
          refresh: "If you have just uploaded a dataset and it does not appear in the list, you can request a refresh of the list. If you look again the dataset will probably already be in the list.",
          selectedData: "If you have selected a dataset from the list, this will show you a preview and the name of the dataset, so you can see what the dataset is sending next.",
          tokenize: "A node that represents a tokenization task. If you don't know anything about tokenization try looking here https://www.machinelearningplus.com/nlp/what-is-tokenization-in-natural-language-processing/",

          availableColumns: "Datasets are a list of columns. These columns represent rows, or some other separation of data. These columns are named and you can have multiple columns next to each other in a dataset. Here is the list of available columns in the node's dataset. If it doesn't have any columns, it's because there is no data flowing into the node",
          taskLanguage: "The environment language and the task language are two different things. The task language only determines what algorithm is used to process the task. In fact, some tasks are different for different languages.",
          targetColumn: "In which column the result of the task should be written. This either creates a new column or renames the column in use.",
          inplaceColumn: "Whether the given column should be replaced. If activated, the result will be only one column, if deactivated, the name of the new column must be specified and the result of the task will be two columns (with the original data and with the new data)",
          newColumn: "If the columns are not to be replaced, the name of the new column must be entered.",
          dropColumns: "Some tasks allow you to remove unnecessary columns.",
          runPipeline: "The application supports batch and automatic mode. Batch mode is enabled by default. In this mode, the user can freely add and join nodes without data flow. You can prepare everything in advance and when you are sure you can run the whole graph with this button. The graph is started from the input nodes and once their successors are processed, the data is sent on until the graph finally arrives. When the graph is running, it is not allowed to add new nodes or link nodes, nor is it recommended to change the node settings. When the graph is started, the graph run indicator appears in this button, until it disappears the graph is still running. Messages about the run progress are written to the log. The run is terminated when no graph is in a running state (yellow). So it is possible for nodes to end up OK (green) or end up with an error (red), unconnected nodes can also remain blue. If the graph takes an unreasonably long time to run, a modal window will appear where you can choose to terminate the process or wait longer.",
          upload: "This button opens a modal window that offers to upload a custom dataset. To upload, you must first name the dataset. You can then upload any text file. Once uploaded to the server, a confirmation message will appear. The dataset should be uploaded and appear among the other datasets. You may need to use the refresh button in the dataset node." ,
          download: "The button opens a modal window that allows you to select the datasets you want to download to your device. In order to download a dataset it must be saved (uploaded, or the saved result of one of the jobs).",
          ending: "We hope everything was clear. The app is still under development, so it is possible that you will run into some bugs. In that case, we would be glad if you could point out the error to us. Thank you.",
          
      },
  },
  runningMode:{
      tooLongProcessing:"Proces trvá nečekaně dlouho, chcete uoknčit proces nebo dále čekat?",
      title:"Nečekaně dlouhý proces"
  }

};

// key="6">{{translate(lang,"layout.addParent")}}<
// ()" key="7">{{translate(lang,"layout.showEdges")
// {{translate(lang,"layout.fitView")}}</a-menu-ite
