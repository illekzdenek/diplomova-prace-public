import { createApp } from 'vue'
import App from './App.vue'
import '@braks/vue-flow/dist/style.css';

import { createPinia } from 'pinia'
import '@braks/vue-flow/dist/theme-default.css';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
export const pinia = createPinia()
const app = createApp(App) 
app.use(Antd);
app.use(pinia)

app.mount('#app')