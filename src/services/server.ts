import axios from "axios";
import { useStore } from "../stores/globalStore";




export async function getApi(url: string, id: string = "", config = null) {
  let defaultConfig = {}
  defaultConfig = {
    headers: {
      'Authorization': 'Bearer ' + useStore().activeUser.token
    }
  }

  if (id.length > 0) {
    url = url + id;
  }
  return axios
    .get(url, config !== null ? config : defaultConfig)
    .then((response) => {
      return response.data;
    })
    .catch(function (error) { });
}
export async function postApi(url: string, body: {}, config = null) {
  //ošetření login požadavku (jediný bez tokenu)
  let defaultConfig = {}
  if (config === null) {
    defaultConfig = {
      headers: {
        'Authorization': 'Bearer ' + useStore().activeUser.token
      }
    }
  }

  return axios
    .post(url, body, config !== null ? config : defaultConfig)
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      return error.response;
    });
}
