import * as nodeTypes from "../constants/nodeTypes";
import { InputPreset } from "../models/nodes/InputPreset";
import { InputCopy } from "../models/nodes/InputCopy";
import { OutputLogConsole } from "../models/nodes/OutputLogConsole";
import { OutputChart } from "../models/nodes/OutputChart";
import { PreCleaning } from "../models/nodes/PreCleaning";
import { PreSelect } from "../models/nodes/PreSelect";
import { InputServer } from "../models/nodes/InputServer";
import { PreServerTokenize } from "../models/nodes/PreServerTokenize";
import { PreStopwords } from "../models/nodes/PreStopwords";
import { PreLemmatize } from "../models/nodes/PreLemmatize";
import { PrePosTagger } from "../models/nodes/PrePosTagger";
import { PreDropColumns } from "../models/nodes/PreDropColumns";
import { PreProcess } from "../models/nodes/PreProcess";

export const buildNode = (type: string, flowNodeId: string) => {
    //console.log(type)
    switch (type) {
        case nodeTypes.INPUT_PRESET:
            return new InputPreset(flowNodeId)
        case nodeTypes.INPUT_COPY:
            return new InputCopy(flowNodeId)

        case nodeTypes.PRE_CLEANING:
            return new PreCleaning(flowNodeId)

        case nodeTypes.PRE_SELECT:
            return new PreSelect(flowNodeId)

        case nodeTypes.PRE_TOKENIZE:
            return new PreServerTokenize(flowNodeId)

        case nodeTypes.PRE_STOP_WORDS:
            return new PreStopwords(flowNodeId)

        case nodeTypes.PRE_LEMMATIZE:
            return new PreLemmatize(flowNodeId)

        case nodeTypes.PRE_POS_TAGGER:
            return new PrePosTagger(flowNodeId)

        case nodeTypes.PRE_DROP_COLUMNS:
            return new PreDropColumns(flowNodeId)

        case nodeTypes.PRE_PRE_PROCESS:
            return new PreProcess(flowNodeId)

        case nodeTypes.INPUT_SERVER:
            return new InputServer(flowNodeId)

        case nodeTypes.OUTPUT_LOG_CONSOLE:
            return new OutputLogConsole(flowNodeId)

        case nodeTypes.OUTPUT_CHART:
            return new OutputChart(flowNodeId)

        default:
            return null
    }
}

export const rebuildSavedNode = (savedData: {}) => {
    let newNode = buildNode(savedData.nodeType, savedData.flowNodeId)
    newNode.data = savedData.data
    newNode.inputs = savedData.inputs
    newNode.outputs = savedData.outputs
    newNode.dataColumns = savedData.dataColumns
    newNode.status = savedData.status
    newNode.statusMessage = savedData.statusMessage
    newNode.errorHandlingType = savedData.errorHandlingType
    switch (savedData.nodeType) {
        case nodeTypes.INPUT_PRESET:
            break;
        case nodeTypes.INPUT_COPY:
            break;

        case nodeTypes.PRE_CLEANING:
            break;

        case nodeTypes.PRE_SELECT:
            newNode.selectedColumns = savedData.selectedColumns
            break;
        case nodeTypes.PRE_TOKENIZE:
            newNode.language = savedData.language
            newNode.targetColumn = savedData.targetColumn
            newNode.inplaceColumn = savedData.inplaceColumn
            newNode.newColumn = savedData.newColumn
            newNode.columnsToDrop = savedData.columnsToDrop
            break;
        case nodeTypes.PRE_STOP_WORDS:
            newNode.language = savedData.language
            newNode.targetColumn = savedData.targetColumn
            newNode.inplaceColumn = savedData.inplaceColumn
            newNode.newColumn = savedData.newColumn
            newNode.columnsToDrop = savedData.columnsToDrop
            break;
        case nodeTypes.PRE_LEMMATIZE:
            newNode.language = savedData.language
            newNode.targetColumn = savedData.targetColumn
            newNode.inplaceColumn = savedData.inplaceColumn
            newNode.newColumn = savedData.newColumn
            newNode.columnsToDrop = savedData.columnsToDrop
            break;
        case nodeTypes.PRE_POS_TAGGER:
            newNode.language = savedData.language
            newNode.targetColumn = savedData.targetColumn
            newNode.inplaceColumn = savedData.inplaceColumn
            newNode.newColumn = savedData.newColumn
            newNode.columnsToDrop = savedData.columnsToDrop
            break;
        case nodeTypes.PRE_DROP_COLUMNS:
            newNode.columnsToDrop = savedData.columnsToDrop
            break;

            
        case nodeTypes.PRE_PRE_PROCESS:
            newNode.language = savedData.language
            newNode.targetColumn = savedData.targetColumn
            newNode.inplaceColumn = savedData.inplaceColumn
            newNode.newColumn = savedData.newColumn
            newNode.columnsToDrop = savedData.columnsToDrop

            newNode.removeStopwords = savedData.removeStopwords
            newNode.useCustomStopwords = savedData.useCustomStopwords
            newNode.customStopwords = savedData.customStopwords
            newNode.lemmatize = savedData.lemmatize
            newNode.toLowercase = savedData.toLowercase
            newNode.removeNumbers = savedData.removeNumbers
            newNode.removeWordsContainingNumbers = savedData.removeWordsContainingNumbers
            newNode.removeEmoticons = savedData.removeEmoticons
            newNode.removeAccents = savedData.removeAccents
            newNode.trimWhitespaces = savedData.trimWhitespaces
            newNode.tokenizeResult = savedData.tokenizeResult
            break;

        case nodeTypes.OUTPUT_LOG_CONSOLE:
            break;

        case nodeTypes.OUTPUT_CHART:
            newNode.selections = savedData.selections
        default:
            break;
    }
    return newNode
}

export const cloneClassInstance = (instance) => {

    const instanceClone = instance.clone()
    // const instanceClone = Object.assign(Object.create(Object.getPrototypeOf(instance)), instance)
    return instanceClone
}
