

import { lessVariablesGreen, lessVariablesYellowGreen, lessVariablesRed, lessVariablesPastel, lessVariablesUniversity, lessVariablesOriginal } from "../constants/lessVariables"

const pallete: string = "original"

export const capitalizeFirstLetter = ([first, ...rest]: any[], locale = navigator.language) =>
    first.toLocaleUpperCase(locale) + rest.join('')

export const arrayTypeToString = (array) => {
    return array.join(" | ")
}

export const getReadableTokens = (dataset, charactersMax = null) => {
    if (dataset && dataset !== null) {
        let readableDataset = ""
        for (let line of dataset) {
            readableDataset += arrayTypeToString(line.text) + "\r\n"
        }
        return readableDataset
    }
    return "No data"
}

export const getReadableDataset = (dataset, charactersMax = null) => {
    if (dataset && dataset !== null && dataset.hasOwnProperty("payload")) {
        let readableDataset = "**" + dataset.name + "**"
        for (let line of dataset.payload) {
            readableDataset = readableDataset + "\r\n" + line.text
            if (charactersMax !== null && readableDataset.length >= charactersMax) {
                readableDataset += "..."
                return readableDataset
            }
        }
        return readableDataset
    }
    return "No data"
}

// export const getColorPalette = () => {
//     switch (pallete) {
//         case "green":
//             return lessVariablesGreen
//         case "yellowgreen":
//             return lessVariablesYellowGreen
//         case "red":
//             return lessVariablesRed
//         case "pastel":
//             return lessVariablesPastel
//         case "university":
//             return lessVariablesUniversity
//         case "original":
//             return lessVariablesOriginal
//         default:
//             return {
//                 mainColor: getRandomColor(),
//                 mainColorLight: getRandomColor(),
//                 lightColor: getRandomColor(),
//                 darkColor: getRandomColor(),
//                 darkColorSecondary: getRandomColor(),
//                 differentColor: getRandomColor(),
//                 white: "white",


//                 secondaryColor: getRandomColor(),
//                 secondaryColorLight: getRandomColor(),
//                 secondaryColorDark: getRandomColor(),

//                 gradient: "linear-gradient(45deg, #00aeff, #a68eff)",
//             }
//     }
// }

// const getRandomColor = () => {
//     return '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
// }