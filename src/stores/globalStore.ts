import { defineStore } from 'pinia'

import { useStorage } from '@vueuse/core'
import tutorialData from "../../data/tutorial.json"

const localStorage = useStorage("settings", {})
// useStore could be anything like useUser, useCart
// the first argument is a unique id of the store across your application
export const useStore = defineStore('main', {
    state: () => ({
        flowInstance: null,
        log: [],
        menuOpen: false,
        flowFunctions: {},
        activeFlowId: "",
        activeUser: null,
        settings:localStorage?localStorage.value:{
            displayActionButtons:false
        },
        acceptModalVisible: false,
        acceptModalMessage: "Are you sure?",
        acceptModalTitle: "Warning",
        acceptModalCallback: ()=>{},
        history: [],
        historyIndex: null,
        pipelineRunning: false,
        showOnboardingTutorial: false,

    }),
    actions: {
        setFlowInstance(payload) {
            this.flowInstance = payload
        },
        addLog(payload){
            //console.log(payload)
            this.log.push({
                message:payload.message, 
                formated:payload.formated
            })
                
        },
        menuSwitch(payload){
            this.menuOpen = payload
        },
        addToFlowFunctions(payload){
            this.flowFunctions[payload.name] = payload.function 
        },
        setActiveFlowId(payload){
            this.activeFlowId = payload
        },
        setActiveUser(payload){
            this.activeUser = payload
        },
        setSettings(payload){
            if (payload.nodeStyle && this.flowFunctions.refreshFlow){
                this.flowFunctions.refreshFlow()
            }
            const newSettings = {...this.settings, ...payload}
            this.settings = newSettings
            
            localStorage.value = newSettings
            
        },
        setAcceptModal(payload){
            if (this.settings.acceptModalAllowed) {
                this.acceptModalVisible = payload.visible
                this.acceptModalMessage = payload.message
                this.acceptModalTitle = payload.title
                this.acceptModalCallback = payload.callback
            } else {
                payload.callback()
            }
        },
        setHistory(payload){
            this.history = payload
        },
        addToHistory(payload){
            this.history.push(payload)
        },
        setHistoryIndex(payload){
            this.historyIndex = payload
        },
        setPipelineRunning(payload){
            this.pipelineRunning = payload
        },
        setShowOnboardingTutorial(payload){
            window.localStorage.setItem("flow-Tab 1", JSON.stringify(tutorialData))
            this.showOnboardingTutorial = payload
        }
    },
})