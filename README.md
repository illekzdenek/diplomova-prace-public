# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's Take Over mode by following these steps:

1. Run `Extensions: Show Built-in Extensions` from VS Code's command palette, look for `TypeScript and JavaScript Language Features`, then right click and select `Disable (Workspace)`. By default, Take Over mode will enable itself if the default TypeScript extension is disabled.
2. Reload the VS Code window by running `Developer: Reload Window` from the command palette.

You can learn more about Take Over mode [here](https://github.com/johnsoncodehk/volar/discussions/471).


# 0.0.1 version

First release with basic functionality. Four nodes jsonInput, presetInput, cleaningData, outputConsoleLog.
Features:
 - BarChart (node detail) show active data on input
 - Node detail is opened after double-click on node. Detail contains table with data, BarChart, Actions defined by logic node & settings (color,name)
 - Edges detail (similar to node detail)
 - Keyboard shortcuts:
    - ctrl + s : save active flow
    - ctrl + d : duplicate active node
 - Badges - show type of node
 - Flow control - close/open menu that contains nodes, active flow save, restore last saved flow
 - Staticaly set 3 tabs (3 diferrent flows)
 - Double language ready (switch + translating mechanism)
 - Footer input/output preview of selected node
 - Log prepared mechanism of logging history
 - Edges have button for progress - that means send data from source to target if data is 100% complete. They have remove button and detail button. Edges can be re-set that means change source or target by grab next to source/target handle
 - Json node - have textarea where JSON can be placed in format "columns", "data" as predefinned value is set
 - Node menu - opened from flow control, by dragging chosen node to flow canvas, will be added to flow