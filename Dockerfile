FROM node:16.12.0-alpine as build
WORKDIR /usr/app

COPY package*.json ./
RUN npm install

#ARG API_URL
#ENV VUE_APP_API_URL $API_URL

COPY . ./
RUN npm run build

# Serve
FROM nginx:1.15-alpine
COPY --from=build /usr/app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
