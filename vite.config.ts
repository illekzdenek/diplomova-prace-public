import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// import { getColorPalette } from "./src/helpers/globalHelper"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: "./",
  css: {
    preprocessorOptions: {
      less: {
        Math: "Alway", // Use mathematics calculation in parentheses
        // globalVars: getColorPalette(),
      },
    },
  },
})
